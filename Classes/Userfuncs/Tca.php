<?php
namespace Maagit\Maagitevent\Userfuncs;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Userfuncs
	class:				Tca

	description:		Tca userfuncs for custom titles in typo3 backend.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version
						2024-10-01	Urs Maag		Call "getFormattedDateTime" with
													0, when it's null

------------------------------------------------------------------------------------- */


class Tca
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * get custom title for tx_maagitevent_domain_model_date
	 *
	 * @param	array			$parameters			the parameter array by reference
	 * @param	object			$parentObject		the parent object
	 * @return	array								the changed parameter array
     */
	public function dateTitle(&$parameters, $parentObject)
    {
		$record = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord($parameters['table'], $parameters['row']['uid']);
        if ($record)
		{
			$from = $this->getFormattedDateTime($record['fromdate']??0);
			$to = $this->getFormattedDateTime($record['todate']??0);
			$newTitle = $this->getFromToDateTimeString($from, $to);
			$newTitle = $this->addLocation($newTitle, $record['location']);
			$newTitle = $this->addRepeatInfo($newTitle, $record['repeat']);
			$newTitle = $this->addSeveralDaysInfo($newTitle, (!in_array($record['individuals'], array('', '0'))));
			$parameters['title'] = $newTitle;
    	}
	}

	/**
     * get custom title for tx_maagitevent_domain_model_except
	 *
	 * @param	array			$parameters			the parameter array by reference
	 * @param	object			$parentObject		the parent object
	 * @return	array								the changed parameter array
     */
	public function exceptTitle(&$parameters, $parentObject)
    {
		$record = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord($parameters['table'], $parameters['row']['uid']);
        if ($record)
		{
			$from = $this->getFormattedDateTime($record['exceptdatefrom']??0);
			$to = $this->getFormattedDateTime($record['exceptdateto']??0);
			$newTitle = $this->getFromToDateTimeString($from, $to);
	        $parameters['title'] = $newTitle;
		}
    }

	/**
     * get custom title for tx_maagitevent_domain_model_individual
	 *
	 * @param	array			$parameters			the parameter array by reference
	 * @param	object			$parentObject		the parent object
	 * @return	array								the changed parameter array
     */
	public function individualTitle(&$parameters, $parentObject)
    {
		$record = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord($parameters['table'], $parameters['row']['uid']);
        if ($record)
		{
			$from = $this->getFormattedDateTime($record['individualdatefrom']??0);
			$to = $this->getFormattedDateTime($record['individualdateto']??0);
			$newTitle = $this->getFromToDateTimeString($from, $to);
			$parameters['title'] = $newTitle;
    	}
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * get array with formatted date and time strings of given unixtimestamp
	 *
	 * @param	int				$timestamp			the unixtimestamp
	 * @return	array								the formatted strings in array:
	 *												- array[0] = 'd.m.Y'
	 *												- array[1] = 'H:i'
     */
	protected function getFormattedDateTime(int $timestamp)
	{
		$dateService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateService');
		if ($timestamp > 0)
		{
			$dateString = $dateService->getDateTime($timestamp)->format('d.m.Y');
			$timeString = $dateService->getDateTime($timestamp)->format('H:i');
		}
		else
		{
			$dateString = '';
			$timeString = '';
		}
		return array(0 => $dateString, 1 => $timeString);
	}

	/**
     * get date/time "<from> - <to>" string
	 *
	 * @param	array				$from			the "from" date/time as array (0=date / 1=time)
	 * @param	array				$to				the "to" date/time as array (0=date / 1=time)
	 * @return	string								the formatted <from>-<to> date/time string
     */
	protected function getFromToDateTimeString(array $from, array $to)
	{
		$string = '';
		$string = $this->getDateTimeString($from[0], $from[1]);
		if ($to[0] != '')
		{
			// date is !=
			if ($from[0] != $to[0])
			{
				$string = $this->getDateTimeString($from[0], $from[1]).' - '.$this->getDateTimeString($to[0], $to[1]);
			}
			// date is ==, time is !=
			if ($from[0] == $to[0] && $from[1] != $to[1])
			{
				$string = $this->getDateTimeString($from[0], $from[1]).' - '.$to[1];
			}
		}
		return $string;
	}

	/**
     * make a date/time string and return it
	 *
	 * @param	string			$date				the date
	 * @param	string			$time				the time
	 * @return	string								the date/time string
     */
	protected function getDateTimeString(string $date, string $time)
	{
		if ($time != '')
		{
			return $date.' '.$time;
		}
		return $date;
	}

	/**
     * check, if there are location settings declared and add this info to given caption
	 *
	 * @param	string			$caption			the caption
	 * @param	string			$location			the location
	 * @return	string								the new caption
     */
	protected function addLocation(string $caption, string $location)
	{
		if (trim($location) != '')
		{
			$text = trim($location); 
			return $caption .= ', '.$text;
		}
		return $caption;
	}

	/**
     * check, if there are repeating date settings declared and add this info to given caption
	 *
	 * @param	string			$caption			the caption
	 * @param	string			$repeat				the repeat information
	 * @return	string								the new caption
     */
	protected function addRepeatInfo(string $caption, string $repeat)
	{
		if ($repeat == 'every')
		{
			$text = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xlf:tca.repeatInfo', 'maagitevent'); 
			return $caption .= ', '.$text;
		}
		return $caption;
	}

	/**
     * check, if there are several day settings declared and add this info to given caption
	 *
	 * @param	string			$caption			the caption
	 * @param	bool			$severalDays		are there several days declared?
	 * @return	string								the new caption
     */
	protected function addSeveralDaysInfo(string $caption, bool $severalDays)
	{
		if ($severalDays)
		{
			$text = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xlf:tca.severalDaysInfo', 'maagitevent'); 
			return $caption .= ', '.$text;
		}
		return $caption;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}