<?php
namespace Maagit\Maagitevent\Userfuncs;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Userfuncs
	class:				Form

	description:		Form userfuncs for getting variables to set the default values
						in the tx_form extension.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version
						2021-09-20	Urs Maag		ObjectManager removed
						2022-10-10	Urs Maag		Typo3 12.0.0 compatibility
													- add method "setContentObjectRenderer"

------------------------------------------------------------------------------------- */


class Form
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	use \Maagit\Maagitevent\General;


	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitevent\Domain\Repository\EventRepository
     */
    protected $eventRepository;
	
	/**
	 * @var \Maagit\Maagitevent\Service\Content\RenderPlacholderService
     */
    protected $renderPlaceholderService;

	/**
	 * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
	 */
	private $cObj;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * receives the current contentObjectRenderer and set's the member variable
	 *
	 * @param	\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer				$cObj			the current contentObjectRenderer
     * @return	void
     */
	public function setContentObjectRenderer(\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer $cObj): void
	{
		$this->cObj = $cObj;
	}

	/**
     * get event object with given id from url parameter and return the placeholder
	 *
	 * @param	string				$content			empty string (no content to process)
     * @param	array           	$conf				typoScript configuration
     * @return	string									the userfunc result
     */
    public function getVariable($content, array $conf)
    {
		// initialize
		$eventid = $this->cObj->stdWrapValue('eventid', $conf, null);
		if (empty($eventid)) {return '';}
		$placeholder = $this->cObj->stdWrapValue('placeholder', $conf, null);
		if (empty($placeholder)) {return '';}
		$this->createEventRepository();
		$this->createRenderPlaceholderService();
		// get event object and render given placeholder
		$event = $this->eventRepository->findById($eventid);
		if (empty($event)) {return '';}
		$value = $this->renderPlaceholderService->renderPlaceholder($placeholder, $event);

		// return result
		return $value;
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * instantiate member variable "eventRepository"
	 *
	 * @param	-
     * @return	void
     */
	protected function createEventRepository()
	{
		if (!$this->eventRepository instanceof \Maagit\Maagitevent\Domain\Repository\EventRepository)
		{
			$this->eventRepository = $this->makeInstance('Maagit\\Maagitevent\\Domain\\Repository\\EventRepository');
		}
	}

	/**
     * instantiate member variable "renderPlaceholderService"
	 *
	 * @param	-
     * @return	void
     */
	protected function createRenderPlaceholderService()
	{
		if (!$this->renderPlaceholderService instanceof \Maagit\Maagitevent\Service\Content\RenderPlacholderService)
		{
			$this->renderPlaceholderService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Content\\RenderPlaceholderService');
		}
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}