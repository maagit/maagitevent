<?php
namespace Maagit\Maagitevent\Service\Property;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Service
	class:				PropertyService

	description:		Various methods for property mapping between a database record
						and a model (e.g. tt_content and "event").

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class PropertyService extends \Maagit\Maagitevent\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * loop given array and set the properties in given domain model
     *
     * @param	\Maagit\Maagitevent\Domain\Model\BaseModel			$model				the model object
	 * @param	array												$values				the values as array
	 * @return	\Maagit\Maagitevent\Domain\Model\BaseModel								the model with properties set
     */
	public function setProperties(\Maagit\Maagitevent\Domain\Model\BaseModel $model, array $values)
	{
		$reflect = new \ReflectionClass(get_class($model));
		foreach ($values as $key => $value)
		{
			try
			{
				$propertyType = $this->getPropertyTypeFromComment($reflect->getProperty(lcfirst($this->getPropertyName($key))));
			}
			catch (\Exception $ex)
			{
				$propertyType = '';
			}
			
			$method = $this->getPropertyMethodName($key);
			if (method_exists($model, $method))
			{
				$value = $this->convertPropertyValueToType($value, $propertyType);
				$model->$method($value);
			}
		}
		return $model;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * create the property "set" method from given member variable
     *
     * @param	string			$key			the name of member variable
	 * @return	string							the property method name
     */
	protected function getPropertyMethodName($key)
	{
		$method = 'set'.$this->getPropertyName($key);
		return $method;
	}
	
	/**
     * get the property name of a given member variable name
     *
     * @param	string			$member			the name of the member variable
	 * @return	string							the property name
     */
	protected function getPropertyName($member)
	{
		if (in_array($member, $this->getExtensionFields()))
		{
			$member = str_replace('tx_maagitevent_', '', $member);
		}
		$propertyName = str_replace('_', '', ucwords($member, '_'));
		return $propertyName;
	}

	/**
     * get the relevant extension columns
     *
     * @param	-
	 * @return	array							the extension columns
     */
	protected function getExtensionFields()
	{
		$extensionFields = array();
		$tcaColumns = $GLOBALS['TCA']['tt_content']['columns'];
		foreach ($tcaColumns as $tcaColumn => $settings)
		{
			if (substr($tcaColumn, 0, 15) == 'tx_maagitevent_')
			{
				$extensionFields[] = $tcaColumn;
			}
		}
		return $extensionFields;
	}
	
	/**
     * get property type from property comment
     *
     * @param	ReflectionProperty		$property			the property
	 * @return	string										the property type
     */
	protected function getPropertyTypeFromComment(\ReflectionProperty $property)
	{
		if (preg_match('/@var\s+([^\s]+)/', $property->getDocComment(), $matches)) {
			return $matches[1];
		}
		return null;
	}
	
	/**
     * converts the DB-value to given property type
     *
     * @param	mixed			$value					the value to convert
	 * @param	string			$type					the destination type
	 * @return	mixed									the converted value
     */
	protected function convertPropertyValueToType($value, string $type)
	{
		// convert to array
		if ($type == 'array')
		{
			return implode(',', $value);
		}

		// convert to boolean
		if ($type == 'bool')
		{
			if ($value == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		
		// return un-converted value
		return $value;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}