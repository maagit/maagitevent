<?php
namespace Maagit\Maagitevent\Service\Date\DateRepeatingService;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Service
	class:				Last

	description:		Calculate repeating dates, based on given "last day of" settings.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Last extends \Maagit\Maagitevent\Service\Date\DateRepeatingService\DateRepeatingService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
 	/**
      * calculate repeating
      *
 	  * @param	-
      * @return	array										the created DateTime objects
      */
	 public function calculate()
	 {
		 $dates = array();
		 $workDate = $this->dateService->getDateTime($this->rangeBegin->format('U'));
		 while ($workDate <= $this->rangeEnd)
		 {
			 if ($this->interval == 'week')
			 {
				 $newDate = $this->dateService->getLastDayOfWeek($workDate);
				 $dates = $this->divHelper->arrayMerge($newDate, $dates);
			 }
			 if ($this->interval == 'month')
			 {
				 $newDate = $this->dateService->getLastDayOfMonth($workDate);
				 $dates = $this->divHelper->arrayMerge($newDate, $dates);
				 $lastOfMonthCalculation = $this->dateService->getDateTime($newDate->format('U'))->modify('-3 day');
				 $workDate = $this->dateService->getDateTime($lastOfMonthCalculation->format('U'));
			 }
			 if ($this->interval == 'year')
			 {
				 $newDate = $this->dateService->getLastDayOfYear($workDate);
				 $dates = $this->divHelper->arrayMerge($newDate, $dates);
			 }
			 $workDate->modify('+'.$this->every.' '.$this->interval);
		 }
		 return $dates;
	 }


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
