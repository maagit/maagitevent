<?php
namespace Maagit\Maagitevent\Service\Date\DateRepeatingService;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Service
	class:				Weekday

	description:		Calculate repeating dates, based on given weekdays.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Weekday extends \Maagit\Maagitevent\Service\Date\DateRepeatingService\DateRepeatingService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
 	/**
      * calculate repeating
      *
 	  * @param	-
      * @return	array										the created DateTime objects
      */
	 public function calculate()
	 {
		 $dates = array();
		 $workDate = $this->dateService->getDateTime($this->rangeBegin->format('U'));
		 while ($workDate <= $this->rangeEnd)
		 {
			 foreach ($this->weekdays as $weekday)
			 {
				 if ($this->last)
				 {
					 $newDate = $this->dateService->getDateTime($workDate->format('U'));
					 $calcDate = $this->dateService->getDateTime($workDate->format('U'));
					 $newDate->modify('last '.$this->dateService->getWeekdayNameByNumber($weekday).' of this '.$this->interval);
					 $newDate = $this->dateService->addTime($newDate, $workDate);
					 if (!($newDate < $this->rangeBegin))
					 {
					 	$dates = $this->divHelper->arrayMerge($newDate, $dates);	
					 }
				 }
				 else
				 {
					 $newDate = $this->dateService->getFirstDayOfWeek($workDate);
					 $newDate->modify('+'.$weekday.' day');
					 if ($newDate < $this->rangeBegin)
					 {
						 $newDate->modify('+7 day');
						 $workDate = $this->dateService->getDateTime($newDate->format('U'));
					 }
					 $dates = $this->divHelper->arrayMerge($newDate, $dates);
				 }
			 }
			 $workDate->modify('+'.$this->every.' '.$this->interval);
		 }
		 return $dates;
	 }


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
