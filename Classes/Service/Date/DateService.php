<?php
namespace Maagit\Maagitevent\Service\Date;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Service
	class:				DateService

	description:		Various methods, used for calculations with date and time.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class DateService extends \Maagit\Maagitevent\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
 	/**
      * get a DateTime Object from given unixtimestamp
      *
 	  * @param	int					$unixtimestamp				the date as unix timestamp
	  * @param	bool				$cutTime					cut the time?
      * @return	\DateTime										the DateTime object
      */
	 public function getDateTime(int $unixtimestamp, bool $cutTime=false)
	 {
		 $dateTime = new \DateTime();
		 $dateTime->setTimestamp($unixtimestamp);
		 if ($cutTime)
		 {
			 $dateTime = $this->cutTime($dateTime);
		 }
		 return $dateTime;
	 }
	 
	 /**
	  * convert a given date range to string
	  *
	  * @param	\DateTime			$from						the from date/time
	  * @param	\DateTime			$to							the to date/time
	  * @param	string				$fromDateFormat				the "from date" format
	  * @param	string				$fromTimeFormat				the "from time" format
	  * @param	string				$toDateFormat				the "to date" format
	  * @param	string				$toTimeFormat				the "to time" format
	  * @param	string				$linebreak					the linebreak string
	  * @return	string											the formatted date/time range string
	  */
	 public function dateRangeToString(\DateTime $from, \DateTime $to, string $fromDateFormat='dd.MM.YYYY', string $fromTimeFormat='HH:mm', string $toDateFormat='dd.MM.YYYY', string $toTimeFormat='HH:mm', string $linebreak='')
 	 {
 		 // initialize
		 $string = '';
		 $fromDate = $this->getDateTimeStringValue($from, $fromDateFormat);
		 $fromTime = $this->getDateTimeStringValue($from, $fromTimeFormat);
 		 $toDate = $this->getDateTimeStringValue($to, $toDateFormat);
		 $toTime = $this->getDateTimeStringValue($to, $toTimeFormat);
		 
		 // format
		 $string = $fromDate.' '.$fromTime;
		 if ($fromDate != $toDate)
		 {
			 if ($fromTime != $toTime || $linebreak != '')
			 {
				 $string .= ' - '.$linebreak.$toDate.' '.$toTime;
			 }
			 else
			 {
				 $string .= ' - '.$toDate;
			 }
		 }
		 if ($fromDate == $toDate && $fromTime != $toTime)
		 {
			 $string .= ' - '.$toTime;
		 }

		 // return formatted string
		 return $string;
 	 }

	 /**
      * check, if given date is in given range
      *
	  * @param	\DateTime	 		$date						the date as unix timestamp
	  * @param	array				$ranges						the date ranges with from- and to-date, contains...
	  *																- array('from' => <\DateTime fromDate>, 'to' => <\DateTime toDate>)
	  * @param	bool	 			$dontConsiderTime			the ending range as unix timestamp
      * @return	bool											is in range?
      */
	 public function isInRange(\DateTime $date, array $ranges, $dontConsiderTime=false)
	 {
		 foreach ($ranges as $range)
		 {
			 $begin = $range['from'];
			 $end = $range['to'];
			 if ($dontConsiderTime)
			 {
				 $begin = $this->cutTime($begin, 0);
				 $end = $this->cutTime($end, 2);	
			 }
			 if ($date >= $begin && $date <= $end)
			 {
				 return true;
			 }
		 }
		 return false;
	 }

	 /**
	  * get all days between a given date range
      *
	  * @param	\DateTime	 		$from						the begin range
	  * @param	\DateTime	 		$to							the end range
	  * @param	bool		 		$exceptWeekend				except weekend days (saturday and sunday)?
      * @return	array											array of dates
      */
	 public function getDaysFromRange(\DateTime $from, \DateTime $to, $exceptWeekend=false)
	 {
		 // get dates and its difference
		 $from = $this->cutTime($from, 0);
		 $to = $this->cutTime($to, 0);
		 $difference = $from->diff($to);
		 $calculationDate = $from;

		 // loop range and add dates per day
		 $days = array();
		 for ($index=0; $index<=$difference->days; $index++)
		 {
			 if (($exceptWeekend && ($calculationDate->format('w') != 0 && $calculationDate->format('w') != 6)) || (!$exceptWeekend))
			 {
			 	$days = $this->divHelper->arrayMerge($this->getDateTime($calculationDate->format('U')), $days);	
			 }
			 $calculationDate->modify('+1 day');
		 }
		 return $days;
	 }

	 /**
	  * get all dates between a given date range with given repeating interval
      *
	  * @param	\DateTime	 		$begin						the begin range
	  * @param	\DateTime	 		$end						the end range
	  * @param	int			 		$every						the interval amount (repeat every ...)
	  * @param	string		 		$interval					the interval (day, week, ...)
	  * @param	arary		 		$weekdays					the relevant weekdays
	  * @param	bool		 		$last						the get "last day of" interval?
      * @return	array											the repeating dates
      */
	 public function getRepeatingDates(\DateTime $begin, \DateTime $end, int $every, string $interval, array $weekdays=array(), bool $last=false)
	 {
		 $dates = array();
		 if (!empty($weekdays))
		 {
			$calculationObject = $this->makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateRepeatingService\\Weekday', $begin, $end, $every, $interval, $weekdays, $last);
			$dates = $this->divHelper->arrayMerge($calculationObject->calculate(), $dates);
		 }
		 if (empty($weekdays) && $last)
		 {
		 	$calculationObject = $this->makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateRepeatingService\\Last', $begin, $end, $every, $interval, $weekdays, $last);
			$dates = $this->divHelper->arrayMerge($calculationObject->calculate(), $dates);
		 }
		 if (empty($weekdays) && !$last)
		 {
		 	$calculationObject = $this->makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateRepeatingService\\Standard', $begin, $end, $every, $interval, $weekdays, $last);
			$dates = $this->divHelper->arrayMerge($calculationObject->calculate(), $dates);
		 }
		 return $dates;
	 }

	 /**
	  * check, if given date is in range of declared except ranges
	  *
 	  * @param	\DateTime			$from			the event date from
 	  * @param	\DateTime			$to				the event date to
 	  * @param	array				$ranges			the date ranges with from- and to-date, contains...
 	  *													- array('from' => <\DateTime fromDate>, 'to' => <\DateTime toDate>)
 	  * @return	bool								is it in except range?
      */
	 public function isInExceptRange(\DateTime $from, \DateTime $to, array $ranges)
	 {
		 // check, event is older than today --> skip it
		 if (!$this->settings['showPastEvents'] && $to->format('U') < time())
		 {
			 return true;
		 }
		 // check declared ranges
		 $days = $this->getDaysFromRange($from, $to);
		 foreach ($days as $day)
		 {
			 if ($this->isInRange($day, $ranges, true))
			 {
				 return true;
			 }
		 }
		 // no except ranges found
 		return false;
	 }

	 /**
	  * get a array with ranges of dates, given from except declarations of event
	  *
	  * @param	\Maagit\Maagitevent\Domain\Model\Event				$event			the event
	  * @return	array																the ranges as array
	  */
	 public function getExceptRanges(\Maagit\Maagitevent\Domain\Model\Event $event)
	 {
		 $ranges = array();
		 foreach ($event->getExcepts() as $except)
		 {
			 $range = array();
			 $range['from'] = $this->getDateTime($except->getExceptdatefrom());
			 $range['to'] = $this->getDateTime($except->getExceptdateto());
			 $ranges[] = $range;
		 }
		 return $ranges;
	 }

	 /**
	  * get date of first day of week from given date
      *
	  * @param	\DateTime	 		$date						the date
      * @return	\DateTime										the date of the first day of week
      */
	 public function getFirstDayOfWeek(\DateTime $date)
	 {
		 $workDate = $this->getDateTime($date->format('U'));
		 $monday = $this->getDateTime($workDate->modify('tomorrow')->modify('previous monday')->format('U'));
		 $monday = $this->addTime($monday, $date);
		 return $monday;
	 }

	 /**
	  * get date of last day of week from given date
      *
	  * @param	\DateTime	 		$date						the date
      * @return	\DateTime										the date of the last day of week
      */
	 public function getLastDayOfWeek(\DateTime $date)
	 {
		 $workDate = $this->getDateTime($date->format('U'));
		 $sunday = $this->getDateTime($this->getFirstDayOfWeek($workDate)->modify('sunday')->format('U'));
		 $sunday = $this->addTime($sunday, $date);
		 return $sunday;
	 }
	 
	 /**
	  * get date of last day of month from given date
      *
	  * @param	\DateTime	 		$date						the date
      * @return	\DateTime										the date of the last day of month
      */
	 public function getLastDayOfMonth(\DateTime $date)
	 {
		 $workDate = $this->getDateTime($date->format('U'));
		 $endOfMonth = $this->getDateTime($workDate->modify('last day of this month')->format('U'));
		 $endOfMonth = $this->addTime($endOfMonth, $date);
		 return $endOfMonth;
	 }
 
	 /**
	  * get date of last day of year from given date
      *
	  * @param	\DateTime	 		$date						the date
      * @return	\DateTime										the date of the last day of month
      */
	 public function getLastDayOfYear(\DateTime $date)
	 {
		 $workDate = $this->getDateTime($date->format('U'));
		 $endOfYear = $this->getDateTime($workDate->modify('last day of December this year')->format('U'));
		 $endOfYear = $this->addTime($endOfYear, $date);
		 return $endOfYear;
	 }

 	/**
 	 * get weekday name
 	 *
 	 * @param	\DateTime				$dateTime		the dateTime
 	 * @param	string					$locale			the locale
 	 * @return	string									the weekday name
 	 */
	public function getWeekdayName(\DateTime $dateTime, string $locale='de_CH')
 	{
		$dateTimeFormatter = new \IntlDateFormatter($locale, \IntlDateFormatter::SHORT, \IntlDateFormatter::SHORT);
 		$dateTimeFormatter->setPattern('EEEE');
 	    $weekdayName = $dateTimeFormatter->format($dateTime);
 		return $weekdayName;
 	}

	/**
	 * get the weekday name from a given day number
	 *
	 * @param	int			 		$weekdayNumber				the weekday number (0 = monday - 6 = sunday)
	 * @return	string											the weekday name
	*/
	public function getWeekdayNameByNumber(int $weekdayNumber)
	{
		$date = new \DateTime();
		$monday = $date->modify('next monday');
		$weekdayName = $this->getWeekdayName($date->modify('+'.$weekdayNumber.' day'), 'en_US');
		return $weekdayName;
	}

 	/**
 	 * get month name
 	 *
 	 * @param	\DateTime				$dateTime		the dateTime
 	 * @param	string					$locale			the locale
 	 * @return	string									the weekday name
 	 */
 	public function getMonthName(\DateTime $dateTime, string $locale='de_CH')
 	{
		$dateTimeFormatter = new \IntlDateFormatter($locale, \IntlDateFormatter::SHORT, \IntlDateFormatter::SHORT);
 		$dateTimeFormatter->setPattern('MMMM');
 	    $monthName = $dateTimeFormatter->format($dateTime);
 		return $monthName;
 	}

	/**
	  * add a given time to a given date
      *
	  * @param	\DateTime	 		$date						the date, which becomes the time added
	  * @param	\DateTime	 		$time						the date object with time to add
      * @return	\DateTime										the date with time
      */
	 public function addTime(\DateTime $date, \DateTime $time)
	 {
		 $unixtimestamp = mktime($time->format('G'), $time->format('i'), $time->format('s'), $date->format('m'), $date->format('d'), $date->format('Y'));
		 $datetime = $this->getDateTime($unixtimestamp);
		 return $datetime;
	 }

	 /**
	  * get date value from a given DateTime object
	  *
	  * @param	\DateTime				$dateTime		the dateTime to convert
	  * @param	string					$format			the dateTime format
	  * @return	string									the value as dateTime string
	  */
 	public function getDateTimeStringValue(\DateTime $dateTime, string $format)
	{
		if (strpos($format, '|'))
		{
 			$params = explode('|', $format);
 			$locale = (empty($params[0])) ? 'de_CH' : $params[0];
 			$format = (empty($params[1])) ? 'dd.MM.YYYY' : $params[1];	
 		}
 		else
 		{
 			$locale = 'de_CH';
 			$format = (empty($format)) ? 'dd.MM.YYYY' : $format;	
 		}
 		$dateTimeFormatter = new \IntlDateFormatter($locale, \IntlDateFormatter::SHORT, \IntlDateFormatter::SHORT);
 		$dateTimeFormatter->setPattern($format);
 	    $dateTimeValue = $dateTimeFormatter->format($dateTime);
 		return $dateTimeValue;
 	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
 	/**
      * cut time from a given date
      *
 	  * @param	\DateTime		$date						the date as unix timestamp
	  * @param	int				$rangeType					the type of cutting time:
	  *														0	=	time is 0:0:0
	  *														1	=	time is 0:0:1
	  *														2	=	time is 23:59:59
      * @return	\DateTime									the date object without time
      */
	 protected function cutTime(\DateTime $date, int $rangeType=0)
	 {
		 if ($rangeType == 1)
		 {
		 	$unixtimestamp = mktime(0, 0, 1, $date->format('m'), $date->format('d'), $date->format('Y'));
		 }
		 elseif ($rangeType == 2)
		 {
		 	$unixtimestamp = mktime(23, 59, 59, $date->format('m'), $date->format('d'), $date->format('Y'));
		 }
		 else
		 {
		 	$unixtimestamp = mktime(0, 0, 0, $date->format('m'), $date->format('d'), $date->format('Y'));
		 }
		 return $this->getDateTime($unixtimestamp, false);
	 }


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
