<?php
namespace Maagit\Maagitevent\Service\Content;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Service
	class:				HtmlService

	description:		Various methods, used for html tag manipulations.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class HtmlService extends \Maagit\Maagitevent\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * search given tag and return, if its found or not
	 *
	 * @param	string					$tag				the tag to find
	 * @param	string					$content			the content to search
	 * @return	bool										tag found?
	 */
	public function findTag(string $tag, string $content)
	{
		return ($this->getBeginTag($tag, $content) !== false);
	}

	/**
	 * get a begin tag
	 *
	 * @param	string					$tag				the tag to get
	 * @param	string					$content			the content to search
	 * @return	string										the tag
	 */
	public function getBeginTag(string $tag, string $content)
	{
		preg_match_all('~<([^/][^>]*?)>~', $content, $matches); 
		foreach ($matches[0] as $foundTag)
		{
			$foundTag = strtolower($foundTag);
			if (substr($foundTag, 0, strlen($tag)) == strtolower($tag))
			{
				return $foundTag;
			}
		}
		return false;
	}

	/**
	 * get a full tag with its content
	 *
	 * @param	string					$tag				the tag to get
	 * @param	string					$content			the content to search
	 * @return	string										the tag with its content
	 */
	public function getFullTag(string $tag, string $content)
	{
		preg_match_all('~<'.$tag.'.*?>.*</'.$tag.'>~', $content, $matches); 
		if (!empty($matches[0][0]))
		{
			return $matches[0][0];
		}
		return false;
	}
	
	/**
	 * replace the a tag with given content
	 *
	 * @param	string							$tagname					the html tag name
	 * @param	string							$replaceWith				the replacing content
	 * @param	string							$content					the content to search in
	 * @return	string														the replaced content
	 */
	public function replaceTag(string $tagName, string $replaceWith, string $content)
	{
		$content = preg_replace('~<'.$tagName.'.*?>.*?</'.$tagName.'>~', $replaceWith, $content);
		return $content;
	}

	/**
	 * add a class name to a tag in given content
	 *
	 * @param	string					$tag				the tag to get
	 * @param	string					$classname			the classname to add
	 * @param	string					$content			the content to search
	 * @return	string										the tag with add classname
	 */
	public function addClassToTag(string $tag, string $classname, string $content)
	{
		preg_match_all('~<'.$tag.'.*? (class=".*?").*?>.*</'.$tag.'>~', $content, $matches); 
		$classAvailable = true;
		if (empty($matches[0][0]))
		{
			$classAvailable = false;
			preg_match_all('~(<'.$tag.'.*?>).*</'.$tag.'>~', $content, $matches);
			if (empty($matches[0][0]))
			{
				return false;
			}
		}
		if ($classAvailable)
		{
			$class = $matches[1][0];
			$class = substr($class, 0, strlen($class) - 1).' '.$classname.'"';
			$content = str_replace($matches[1][0], $class, $matches[0][0]);
		}
		else
		{
			$startTag = substr($matches[1][0], 0, strlen($matches[1][0]) - 1).' class="'.$classname.'">';
			$content = str_replace($matches[1][0], $startTag, $matches[0][0]);
		}
		return $content;
	}


 	/* ======================================================================================= */
 	/* P R O T E C T E D   M E T H O D S                                                       */
 	/* ======================================================================================= */


 	/* ======================================================================================= */
 	/* P R I V A T E   M E T H O D S                                                           */
 	/* ======================================================================================= */
}