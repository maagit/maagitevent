<?php
namespace Maagit\Maagitevent\Service\Content;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Service
	class:				RenderPlaceholderService

	description:		Methods to parse and render placholders in content.
						Placeholders are defined as following:
						- ###METHOD_NAME###
						- ###PROPERTY###
						- ###PROPERTY.SUBPROPERTY###
						- ###PROPERTY OR METHOD[FORMAT]###

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version
						2021-09-20	Urs Maag		ObjectManager removed

------------------------------------------------------------------------------------- */


class RenderPlaceholderService extends \Maagit\Maagitevent\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	 /**
	  * replace placeholders
	  *
	  * @param	string 											$text			the text to parse
	  * @param	\Maagit\Maagitevent\Domain\Model\BaseModel		$object			the model
	  * @return	string															the content
	  */
	 public function renderPlaceholder(string $text, \Maagit\Maagitevent\Domain\Model\BaseModel $object)
 	 {
		 // for typolinks --> replace the pipe (%7C) to # and remove the cHash parameter
		 $text = str_replace('%7C%7C%7C', '###', $text);
		 $text = str_replace('%5B', '[', $text);
		 $text = str_replace('%5D', ']', $text);
		 $text = preg_replace('~&amp;cHash=.*?">~', '">', $text);

		 // get placeholders and render it
 		 preg_match_all('/###(.*?)###/', $text, $placeholders);
 		 foreach ($placeholders[0] as $placeholder)
 		 {
 			 // initialize
			 $renderResult = FALSE;
			 $member = str_replace('###', '', $placeholder);

			 // get format rules
			 preg_match_all('/\[(.*?)\]/', $member, $formats);
			 if (!empty($formats[1]))
			 {
				 $format = trim($formats[1][0]);
				 $member = trim(str_replace('['.$format.']', '', $member));
			 }
			 else
			 {
				 $format = '';
			 }

			 // create function calls and property calls of given placeholder
 			 $key = (strpos($member, '.')!== FALSE) ? substr($member, strpos($member, '.')+1) : '';
			 $member = ($key != '') ? substr($member, 0, strpos($member, '.')) : $member;
 			 $functionSegment = str_replace('_', '', ucwords(strtolower($member), '_'));
 			 $function = 'render'.$functionSegment;
 			 $property = 'get'.$functionSegment;

 			 // first prio: call method in this class, if exists
 			 if (method_exists($this, $function))
 			 {
 			 	$renderResult = $this->$function($object, $format);
 			 }
 			 // second prio: call object property
 			 else if (method_exists($object, $property))
 			 {
 				 $renderResult = $object->$property();
 			 }
 			 else
 			 {
 				 $renderResult = FALSE;
 			 }

 			 // get array value, if result is a array
 			 if (is_array($renderResult))
 			 {
 				 $renderResult = $this->getArrayValue($renderResult, $key.'['.$format.']');
 			 }

 			 // get array value, if result is a objectstorage
 			 if ($renderResult instanceof \TYPO3\CMS\Extbase\Persistence\ObjectStorage)
 			 {
 				 $renderResult = $this->getObjectStorageValue($renderResult, $key.'['.$format.']');
 			 }

			 // convert to string, if its a object
			 if (is_object($renderResult))
			 {
				 if ($renderResult instanceof \DateTime)
				 {
					 $renderResult = $this->getDateTimeValue($renderResult, $format);
				 }
			 }
			 
			 // convert to currency, if its a numeric value and format is given
			 if (is_double($renderResult) && $format != '')
			 {
				 $renderResult = $this->getCurrencyValue($renderResult, $format);
			 }

 			 // replace placeholder with given result
 			 if ($renderResult !== FALSE)
 			 {
 				 $text = str_replace($placeholder, $renderResult, $text);
 			 }
 		 }
 		 return $text;
 	 }


 	/* ======================================================================================= */
 	/* P R O T E C T E D   M E T H O D S                                                       */
 	/* ======================================================================================= */
	/**
	 * get dates from location as string
	 *
	 * @param	\Maagit\Maagitevent\Domain\Model\BaseModel			$model				the model
	 * @param	string												$format				the format
	 * @return	string																	the dates as string
	 */
	protected function renderDatelist(\Maagit\Maagitevent\Domain\Model\BaseModel $model, string $format)
	{
		$dateService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateService');
		$dateArray = array();
		$dateString = '';
		if ($model instanceof \Maagit\Maagitevent\Domain\Model\Event)
		{
			$repository = $this->makeInstance($this->divHelper->getRepositoryName($model));
			$events = $repository->findByEvent($model);
			foreach ($events as $event)
			{
				$dateArray[] = $event->getDatefrom();
			}
		}
		if ($model instanceof \Maagit\Maagitevent\Domain\Model\Location)
		{
			foreach ($model->getDates() as $date)
			{
				$dateArray[] = $date;
			}
		}
		if ($model instanceof \Maagit\Maagitevent\Domain\Model\Date)
		{
			foreach ($model->getIndividuals() as $individual)
			{
				$dateArray[] = $individual->getDatefrom();
			}
		}
		sort($dateArray);
		foreach ($dateArray as $date)
		{
			$dateString .= ($dateString!='') ? ' / ' : '';
			$dateString .= $this->getDateTimeValue($date, $format);
		}
		return $dateString;
	}


 	/* ======================================================================================= */
 	/* P R I V A T E   M E T H O D S                                                           */
 	/* ======================================================================================= */
	/**
	 * get value from a array
	 *
	 * @param	array					$array			the array to search in
	 * @param	string					$key			the key(s), delimited by given delimiter
	 * @param	string					$delimiter		the delimiter of keys, default is '.'
	 * @return	string									the value
	 */
	private function getArrayValue(array $array, string $key, string $delimiter='.')
	{
 	 	$temp = $array;
 		$segments = explode($delimiter, $key);
 		foreach ($segments as $segment)
 		{
 			$value = $temp[$segment];
 			$value = ($value == NULL) ? $temp[strtoupper($segment)] : $value;
 			$value = ($value == NULL) ? $temp[strtolower($segment)] : $value;
 			$value = ($value == NULL) ? $temp[ucwords(strtolower($segment))] : $value;
 			$temp = (is_array($value)) ? $value : array();
 		}
 		return $value;
 	}

	/**
	 * get value from a objectstorage
	 *
	 * @param	\TYPO3\CMS\Extbase\Persistence\ObjectStorage			$objectStorage			the object storage to search in
	 * @param	string													$key					the key(s), delimited by given delimiter
	 * @param	string													$delimiter				the delimiter of keys, default is '.'
	 * @return	string													the value
	 */
	private function getObjectStorageValue(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $objectStorage, string $key, string $delimiter='.')
	{
		$temp = $objectStorage;
		if (strpos($key, '['))
		{
			$keyFormat = substr($key, strpos($key, '['));
			$key = substr($key, 0, strpos($key, '['));
			$keyFormat = str_replace('.', '-point-', $keyFormat);
			$key = $key.$keyFormat;
		}
 		$segments = explode($delimiter, $key);
 		foreach ($segments as $segment)
 		{
 			if ($temp instanceof \Maagit\Maagitevent\Domain\Model\BaseModel)
			{
				$segment = str_replace('-point-', '.', $segment);
				$value = $this->renderPlaceholder('###'.$segment.'###', $temp);
			}
			else
			{
				$value = $temp[$segment];
				$value = ($value == NULL) ? $temp[strtoupper($segment)] : $value;
	 			$value = ($value == NULL) ? $temp[strtolower($segment)] : $value;
	 			$value = ($value == NULL) ? $temp[ucwords(strtolower($segment))] : $value;
			}
 			$temp = $value;
 		}
 		return $value;
 	}
	
	/**
	 * get date value from a given DateTime object
	 *
	 * @param	\DateTime				$dateTime		the dateTime to convert
	 * @param	string					$format			the dateTime format
	 * @return	string									the value as dateTime string
	 */
	private function getDateTimeValue(\DateTime $dateTime, string $format)
	{
		$dateService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateService');
		return $dateService->getDateTimeStringValue($dateTime, $format);
	}

	/**
	 * get currency value from a given number
	 *
	 * @param	double					$number			the number to convert
	 * @param	string					$format			the currency format settings:
	 *													- <locale>|<currency>
	 *													- example: de_DE|EUR
	 * @return	string									the value as currency string
	 */
	private function getCurrencyValue(float $number, string $format)
	{
		$params = explode('|', $format);
		if (count($params) < 2) {$params[1] = $params[0]; $params[0] = '';}
		$locale = (empty($params[0])) ? 'de_CH' : $params[0];
		$currency = (empty($params[1])) ? 'CHF' : $params[1];
		$numberFormatter = new \NumberFormatter($locale, \NumberFormatter::CURRENCY);
		$currencyValue = $numberFormatter->formatCurrency($number, $currency);
		return $currencyValue;
	}
}
