<?php
namespace Maagit\Maagitevent\Service\Content;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Service
	class:				RenderFontawesomeService

	description:		Methods to parse and render fontawesome tags in content.

	created:			2022-11-16
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-11-16	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class RenderFontawesomeService extends \Maagit\Maagitevent\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	 /**
	  * replace fontawesome tags.
	  *
	  * Convert text fragments for fontawesome as follows
	  * - '#fa-car fa-solid#' to '<i class="fa fa-car fa-solid"></i>'
	  *
	  * @param	string 											$text			the text to parse
	  * @return	string															the content
	  */
	 public function renderFontawesome(string $text)
 	 {
		 preg_match_all('/#fa-(.*?)#/', $text, $tags);
		foreach ($tags[0] as $tag)
		{
			$tag = str_replace('#', '', $tag);
			$text = str_replace('#'.$tag.'#', '<i class="fa '.$tag.'" aria-hidden="true"></i>', $text);
		}
		return $text;
 	 }


 	/* ======================================================================================= */
 	/* P R O T E C T E D   M E T H O D S                                                       */
 	/* ======================================================================================= */


 	/* ======================================================================================= */
 	/* P R I V A T E   M E T H O D S                                                           */
 	/* ======================================================================================= */
}
