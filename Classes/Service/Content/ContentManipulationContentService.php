<?php
namespace Maagit\Maagitevent\Service\Content;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Service
	class:				ContentManipulationContentService

	description:		Render content element based on object data.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContentManipulationContentService extends \Maagit\Maagitevent\Service\Content\ContentManipulationService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * render method for manipulating content
	 *
	 * @param	-
	 * @return	array										the manipulated objects
	 */
	public function render()
	{
		$array = array();
		$renderService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Content\\RenderService');
		$renderTypolinkService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Content\\RenderTypolinkService');
		$renderFontawesomeService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Content\\RenderFontawesomeService');
		$renderPlaceholderService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Content\\RenderPlaceholderService');
		foreach ($this->events as $event)
		{
			$content = $renderService->renderContentElement($event->getUid());
			if ($event->getCancelled() && $event->getFull())
			{
				$content = '<div class="cancelled full">'.$content.'</div>';
			}
			if ($event->getCancelled() && !$event->getFull())
			{
				$content = '<div class="cancelled">'.$content.'</div>';
			}
			if (!$event->getCancelled() && $event->getFull())
			{
				$content = '<div class="full">'.$content.'</div>';
			}
			$content = $renderTypolinkService->renderTypolink($content);
			$content = $renderFontawesomeService->renderFontawesome($content);
			$content = $renderPlaceholderService->renderPlaceholder($content, $event);
			$event->setContent($content);
			$array = $this->divHelper->arrayMerge($event, $array);
		}
		return $array;
	}


 	/* ======================================================================================= */
 	/* P R O T E C T E D   M E T H O D S                                                       */
 	/* ======================================================================================= */


 	/* ======================================================================================= */
 	/* P R I V A T E   M E T H O D S                                                           */
 	/* ======================================================================================= */
}