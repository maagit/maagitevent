<?php
namespace Maagit\Maagitevent\Service\Content;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Service
	class:				RenderService

	description:		Various methods, used for rendering with fluid.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version
						2021-12-24	Urs Maag		PHP 8, fix non existing array keys
						2022-10-10	Urs Maag		Typo3 12.0.0 compatibility
													- get parameter "$request" and
													  create renderingContext

------------------------------------------------------------------------------------- */


class RenderService extends \Maagit\Maagitevent\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * render a template
     *
	 * @param	string 						$template		The fluid template
	 * @param	array 						$assigns		Key/Value pairs of data's to assign
     * @return	string										the rendered content
	 */
	public function renderTemplate(string $template, array $assigns=array())
	{
		$view = $this->makeInstance('TYPO3\\CMS\\Fluid\\View\\StandaloneView', $this->divHelper->getRenderingContext());
		$template .= (end(explode('.', $template))) ? '' : '.html';
		$view->setTemplate($template);
		$view->setTemplateRootPaths($this->settings['view']['templateRootPaths']);
		$view->setPartialRootPaths($this->settings['view']['partialRootPaths']);
		foreach ($assigns as $key => $data)
		{
			$view->assign($key, $data);
		}
		return $view->render();
	}
	 
	/**
	 * render a content element by typoscript
	 *
	 * @param	int 						$uid			The tt_content uid
	 * @return	string										the rendered content
	 */
	public function renderContentElement(int $uid)
	{
		$conf = array (
			'tables' => 'tt_content',
			'source' => $uid,
			'dontCheckPid' => 1
		);
		$contentObject = $this->makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer');
		$content = $contentObject->cObjGetSingle('RECORDS', $conf);
		return $content;
	}

	/**
	 * parse and render event objects
	 *
	 * @param	array 						$events			The event objects
	 * @return	array										the event objects with rendered content property set
	 */
	public function renderEvents(array $events)
	{
		// render content
		$contentManipulationContentService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Content\\ContentManipulationContentService', $events);
		$events = $contentManipulationContentService->render();

		// remove repeating header
		if (!isset($this->settings['content']['dontRepeatHeader'])) {$this->settings['content']['dontRepeatHeader'] = false;}
		if ($this->settings['content']['dontRepeatHeader'])
		{
			$contentManipulationHeaderService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Content\\ContentManipulationHeaderService', $events);
			$events = $contentManipulationHeaderService->render();	
		}

		// collapse tables
		if (!isset($this->settings['route'])) {$this->settings['route'] = '';}
		if (!isset($this->settings['content']['dontCollapseTable'])) {$this->settings['content']['dontCollapseTable'] = false;}
		if ($this->settings['route']=='listDetail' && !$this->settings['content']['dontCollapseTable'])
		{
			$contentManipulationTableService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Content\\ContentManipulationTableService', $events);
			$events = $contentManipulationTableService->render();	
		}

		// return objects with rendered content
		return $events;
	}

	/**
	 * parse and render event object
	 *
	 * @param	\Maagit\Maagitevent\Domain\Model\Event		$event				The event object
	 * @return	\Maagit\Maagitevent\Domain\Model\Event							the event object with rendered content property set
	 */
	public function renderEvent(\Maagit\Maagitevent\Domain\Model\Event $event)
	{
		$events = $this->renderEvents(array($event));
		return $events[0];
	}


 	/* ======================================================================================= */
 	/* P R O T E C T E D   M E T H O D S                                                       */
 	/* ======================================================================================= */


 	/* ======================================================================================= */
 	/* P R I V A T E   M E T H O D S                                                           */
 	/* ======================================================================================= */
}