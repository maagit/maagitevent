<?php
namespace Maagit\Maagitevent\Service\Content;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Service
	class:				ContentManipulationTableService

	description:		Collapse tables to one table.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContentManipulationTableService extends \Maagit\Maagitevent\Service\Content\ContentManipulationService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitevent\Service\Content\HtmlService
     */
	protected $htmlService;

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Initialize the object
     *
     */
	public function initializeObject(array $events)
	{
		parent::initializeObject($events);
		$this->htmlService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Content\\HtmlService');
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * render method for manipulating content
	 *
	 * @param	-
	 * @return	array										the manipulated objects
	 */
	public function render()
	{
		// initialization
		$array = array();
		$length = count($this->events);
		$previousIsTable = false;
		$table = '';

		// loop content
		for ($index = 0; $index < $length; $index++)
		{
			// get current and next event, for comparing
			$previous = ($index>0) ? prev($this->events) : null;
			$current = ($previous==null) ? current($this->events) : next($this->events);

			// get informations
			$content = $current->getContent();
			$headerChanged = (($index==0) || ($current->getHeader()!=$previous->getHeader()));
			$isTable = $this->htmlService->findTag('<table', $content);

			// close previous table and add event with the table content
			if ($previousIsTable && $headerChanged)
			{
				$table .= '</tbody></table>';
				$array = $this->divHelper->arrayMerge($this->addTableContent($first, $table), $array);
			}
			
			// add event, if it is normal content (not a table)
			if (!$isTable)
			{
				$array = $this->divHelper->arrayMerge($current, $array);
			}
			
			// make table things
			if ($isTable)
			{
				// begin new table
				if ($headerChanged)
				{
					$first = $current;
					$table = $this->htmlService->getBeginTag('<table', $content).'<tbody>';
				}

				// write table row
				$tr = $this->htmlService->getFullTag('tr', $content);
				if ($current->getCancelled() && $current->getFull())
				{
					$tr = $this->htmlService->addClassToTag('tr', 'cancelled full', $tr);
				}
				if ($current->getCancelled() && !$current->getFull())
				{
					$tr = $this->htmlService->addClassToTag('tr', 'cancelled', $tr);
				}
				if (!$current->getCancelled() && $current->getFull())
				{
					$tr = $this->htmlService->addClassToTag('tr', 'full', $tr);
				}
				$table .= $tr;
			}

			// next event
			next($this->events);
			$previousIsTable = $isTable;
		}
		
		// close last table and add event with the table content
		if ($previousIsTable)
		{
			$table .= '</tbody></table>';
			$array = $this->divHelper->arrayMerge($this->addTableContent($first, $table), $array);
		}

		// return new objects
		return $array;
	}


 	/* ======================================================================================= */
 	/* P R O T E C T E D   M E T H O D S                                                       */
 	/* ======================================================================================= */
	/**
	 * replace the table part of content with the new created table content
	 *
	 * @param	\Maagit\Maagitevent\Domain\Model\Event		$event			the event
	 * @param	string										$tableContent	the new created table content
	 * @return	\Maagit\Maagitevent\Domain\Model\Event						the event object
	 */
	protected function addTableContent(\Maagit\Maagitevent\Domain\Model\Event $event, string $tableContent)
	{
		$content = $event->getContent();
		$content = $this->htmlService->replaceTag('table', $tableContent, $content);
		$event->setContent($content);
		return $event;
	}


 	/* ======================================================================================= */
 	/* P R I V A T E   M E T H O D S                                                           */
 	/* ======================================================================================= */
}