<?php
namespace Maagit\Maagitevent\Service\Content;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Service
	class:				RenderTypolinkService

	description:		Methods to parse and render typolinks in content.

	created:			2022-10-16
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-10-16	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class RenderTypolinkService extends \Maagit\Maagitevent\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	 /**
	  * replace typolinks
	  *
	  * @param	string 											$text			the text to parse
	  * @return	string															the content
	  */
	 public function renderTypolink(string $text)
 	 {
		 // get links and render it
		 preg_match_all('/<a href="(.*?)".*?>.*?<\/a>/', $text, $links);
		 foreach ($links[0] as $link)
		 {
			 // parse link
	 		$contentObject = $this->makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer');
			$result = $contentObject->parseFunc($link, array(), '< lib.parseFunc');

 			 // replace link with given result
 			 if ($result !== FALSE)
 			 {
 				 $text = str_replace($link, $result, $text);
 			 }
 		 }
 		 return $text;
 	 }


 	/* ======================================================================================= */
 	/* P R O T E C T E D   M E T H O D S                                                       */
 	/* ======================================================================================= */


 	/* ======================================================================================= */
 	/* P R I V A T E   M E T H O D S                                                           */
 	/* ======================================================================================= */
}
