<?php
namespace Maagit\Maagitevent\Routing\Aspect;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Routing
	class:				PersistedEventMapper

	description:		Maps data from persisted events for speaking url's.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class PersistedEventMapper implements \TYPO3\CMS\Core\Routing\Aspect\PersistedMappableAspectInterface, \TYPO3\CMS\Core\Routing\Aspect\StaticMappableAspectInterface
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var array
     */
    protected $settings;

    /**
     * @var int
     */
    protected $pluginPid;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    /**
     * @param array $settings
     * @throws \InvalidArgumentException
     */
    public function __construct(array $settings)
    {
        $pluginPid = $settings['pluginPid'] ?? null;
        
		if (empty($pluginPid)) {
            throw new \InvalidArgumentException(
                'pluginPid is missed',
                1594794026
            );
        }
        if (!is_numeric($pluginPid)) {
            throw new \InvalidArgumentException(
                'pluginPid is not a number',
                1594794143
            );
        }

        $this->settings = $settings;
        $this->pluginPid = $pluginPid;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     *	generate mapped url path from given url arguments
	 *
	 *	@param	string				$value				the url argument value
	 *	@return	string									the mapped value
     */
    public function generate(string $value): ?string
    {
		$event = $this->createEventRepository()->findById($value);
		$slug = $event->getTitle().'/'.$this->createDateService()->getDateTimeStringValue($event->getDatefrom(), 'YYYY-MM-dd');
		return $this->createSlugHelper()->sanitize($slug);	
    }

	/**
     *	resolve original url argument from mapped url path
	 *
	 *	@param	string				$value				the url path
	 *	@return	string									the id
     */
    public function resolve(string $value): ?string
    {
		$eventRepository = $this->createEventRepository();
		return $eventRepository->findBySlug($value, $this->pluginPid);
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     *	get the event repository
	 *
	 *	@param	-
	 *	@return	\Maagit\Maagitevent\Domain\Repository\EventRepository				the event repository
     */
    protected function createEventRepository(): \Maagit\Maagitevent\Domain\Repository\EventRepository
    {
		$eventRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitevent\\Domain\\Repository\\EventRepository');
		return $eventRepository;
    }

	/**
     *	get the slug helper
	 *
	 *	@param	-
	 *	@return	\TYPO3\CMS\Core\DataHandling\SlugHelper				the slug helper
     */
    protected function createSlugHelper(): \TYPO3\CMS\Core\DataHandling\SlugHelper
    {
        $slugHelper = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\DataHandling\\SlugHelper', '', '', array());
		return $slugHelper;
    }

	/**
     *	get the date service
	 *
	 *	@param	-
	 *	@return	\Maagit\Maagitevent\Service\Date\DateService				the date service
     */
    protected function createDateService(): \Maagit\Maagitevent\Service\Date\DateService
    {
        $dateService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateService');
		return $dateService;
    }


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}