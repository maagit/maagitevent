<?php
namespace Maagit\Maagitevent\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Model
	class:				Event

	description:		Model for the event.
						Inherits the datas of tt_content with type "event"

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Event extends \Maagit\Maagitevent\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var int
     */
    protected ?int $uid;

	/**
	 * @var int
     */
    protected ?int $pid;

	/**
	 * @var string
     */
    protected $id;

	/**
	 * @var string
     */
    protected $title;

	/**
	 * @var int
     */
    protected $dates;
	
	/**
	 * @var \DateTime
     */
    protected $datefrom;
	
	/**
	 * @var \DateTime
     */
    protected $dateto;

	/**
	 * @var int
     */
    protected $individuals;

	/**
	 * @var int
     */
    protected $excepts;

	/**
	 * @var string
     */
    protected $content;

	/**
	 * @var int
     */
    protected $next;
	
	/**
	 * @var double
     */
    protected $price;
	
	/**
	 * @var bool
     */
    protected $cancelled;

	/**
	 * @var bool
     */
    protected $full;

	/**
	 * @var string
     */
    protected $location;

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Location>
     */
    protected $locations;

	/**
	 * @var string
     */
    protected $rowDescription;

	/**
	 * @var string
     */
    protected $CType;

	/**
	 * @var int
     */
    protected $colPos;

	/**
     * @var string
     */
    protected $header;

	/**
     * @var string
     */
    protected $headerLayout;

	/**
     * @var string
     */
    protected $headerPosition;

	/**
	 * @var int
     */
    protected $date;

	/**
     * @var string
     */
    protected $headerLink;

	/**
     * @var string
     */
    protected $subheader;

	/**
     * @var string
     */
    protected $bodytext;

	/**
	 * @var int
     */
    protected $assets;

	/**
	 * @var int
     */
    protected $imagewidth;

	/**
	 * @var int
     */
    protected $imageheight;

	/**
	 * @var bool
     */
    protected $imageborder;

	/**
	 * @var int
     */
    protected $imageorient;

	/**
	 * @var int
     */
    protected $imagecols;

	/**
	 * @var bool
     */
    protected $imageZoom;

	/**
	 * @var int
     */
    protected $layout;

	/**
	 * @var string
     */
    protected $frameClass;

	/**
	 * @var string
     */
    protected $spaceBeforeClass;

	/**
	 * @var string
     */
    protected $spaceAfterClass;

	/**
	 * @var bool
     */
    protected $sectionIndex;

	/**
	 * @var bool
     */
    protected $linkToTop;

	/**
	 * @var int
     */
    protected $sysLanguageUid;

	/**
	 * @var bool
     */
    protected $hidden;

	/**
	 * @var int
     */
    protected $starttime;

	/**
	 * @var int
     */
    protected $endtime;

	/**
	 * @var string
     */
    protected $feGroup;

	/**
	 * @var bool
     */
    protected $editlock;

	/**
	 * @var int
     */
    protected $categories;

   
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		$this->locations = $this->makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage');
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
	 * Set the uid
	 *
	 * @param int $uid
	 */
	public function setUid($uid)
	{
	    $this->uid = $uid;
	}

	/**
	 * Set the id
	 *
	 * @param string	$id
	 */
	public function setId($id)
	{
	    // @extensionScannerIgnoreLine
		$this->id = $id;
	}

	/**
	 * Get the id
	 *
	 * @return string
	 */
	public function getId()
	{
	    // @extensionScannerIgnoreLine
		return $this->id;
	}
	
	/**
	 * Get the root id
	 *
	 * @return string
	 */
	public function getRoot()
	{
	    // @extensionScannerIgnoreLine
		return substr($this->id, 0, strpos($this->id, '_'));
	}

	/**
	 * Set the title
	 *
	 * @param string	$title
	 */
	public function setTitle($title)
	{
	    $this->title = $title;
	}

	/**
	 * Get the title
	 *
	 * @return string
	 */
	public function getTitle()
	{
	    if ($this->title == null || $this->title == '')
		{
			return $this->getHeader();
		}
		return $this->title;
	}

	/**
	 * Get the datefromto
	 *
	 * @return string
	 */
	public function getDateRange()
	{
	    $dateService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateService');
		return $dateService->dateRangeToString($this->getDateFrom(), $this->getDateTo());
	}

	/**
	 * Set the dates
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Date> $dates
	 */
	public function setDates($dates)
	{
	    $this->dates = $dates;
	}

	/**
	 * Get the dates
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Date>
	 */
	public function getDates()
	{
	    return $this->dates;
	}

	/**
	 * Set the individuals
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Individual> $individuals
	 */
	public function setIndividuals($individuals)
	{
	    $this->individuals = $individuals;
	}

	/**
	 * Get the individuals
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Individual>
	 */
	public function getIndividuals()
	{
	    return $this->individuals;
	}

	/**
	 * Set the excepts
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Except> $excepts
	 */
	public function setExcepts($excepts)
	{
	    $this->excepts = $excepts;
	}

	/**
	 * Get the excepts
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Except>
	 */
	public function getExcepts()
	{
	    return $this->excepts;
	}

	/**
	 * Set the datefrom
	 *
	 * @param \DateTime $datefrom
	 */
	public function setDatefrom($datefrom)
	{
	    $this->datefrom = $datefrom;
	}

	/**
	 * Get the datefrom
	 *
	 * @return \DateTime
	 */
	public function getDatefrom()
	{
	    return $this->datefrom;
	}

	/**
	 * Set the dateto
	 *
	 * @param \DateTime $dateto
	 */
	public function setDateto($dateto)
	{
	    $this->dateto = $dateto;
	}

	/**
	 * Get the dateto
	 *
	 * @return \DateTime
	 */
	public function getDateto()
	{
	    return $this->dateto;
	}

	/**
	 * Set the next
	 *
	 * @param string $next
	 */
	public function setNext($next)
	{
	    $this->next = $next;
	}

	/**
	 * Get the next
	 *
	 * @return string
	 */
	public function getNext()
	{
	    return $this->next;
	}

	/**
	 * Set the price
	 *
	 * @param string $price
	 */
	public function setPrice($price)
	{
	    $this->price = $price;
	}

	/**
	 * Get the price
	 *
	 * @return string
	 */
	public function getPrice()
	{
	    return $this->price;
	}
	
	/**
	 * Set the cancelled
	 *
	 * @param bool $cancelled
	 */
	public function setCancelled($cancelled)
	{
	    $this->cancelled = $cancelled;
	}

	/**
	 * Get the cancelled
	 *
	 * @return bool
	 */
	public function getCancelled()
	{
	    return $this->cancelled;
	}

	/**
	 * Set the full
	 *
	 * @param bool $full
	 */
	public function setFull($full)
	{
	    $this->full = $full;
	}

	/**
	 * Get the full
	 *
	 * @return bool
	 */
	public function getFull()
	{
	    return $this->full;
	}

	/**
	 * Set the location
	 *
	 * @param string $location
	 */
	public function setLocation($location)
	{
	    $this->location = $location;
	}

	/**
	 * Get the location
	 *
	 * @return string
	 */
	public function getLocation()
	{
	    return $this->location;
	}

	/**
	 * Set the locations
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Location> $locations
	 */
	public function setLocations($locations)
	{
	    $this->locations = $locations;
	}

	/**
	 * Get the locations
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Location>
	 */
	public function getLocations()
	{
	    return $this->locations;
	}

	/**
	 * Set the content
	 *
	 * @param string $content
	 */
	public function setContent($content)
	{
	    // @extensionScannerIgnoreLine
		$this->content = $content;
	}

	/**
	 * Get the content
	 *
	 * @return string
	 */
	public function getContent()
	{
	    // @extensionScannerIgnoreLine
		return $this->content;
	}

	/**
	 * Set the CType
	 *
	 * @param string $CType
	 */
	public function setCType($CType)
	{
	    $this->CType = $CType;
	}

	/**
	 * Get the CType
	 *
	 * @return string
	 */
	public function getCType()
	{
	    return $this->CType;
	}

	/**
	 * Set the colPos
	 *
	 * @param int $colPos
	 */
	public function setColPos($colPos)
	{
	    $this->colPos = $colPos;
	}

	/**
	 * Get the colPos
	 *
	 * @return int
	 */
	public function getColPos()
	{
	    return $this->colPos;
	}

	/**
	 * Set the header
	 *
	 * @param string $header
	 */
	public function setHeader($header)
	{
	    $this->header = $header;
	}

	/**
	 * Get the header
	 *
	 * @return string
	 */
	public function getHeader()
	{
	    return $this->header;
	}

	/**
	 * Set the headerLayout
	 *
	 * @param string $headerLayout
	 */
	public function setHeaderLayout($headerLayout)
	{
	    $this->headerLayout = $headerLayout;
	}

	/**
	 * Get the headerLayout
	 *
	 * @return string
	 */
	public function getHeaderLayout()
	{
	    return $this->headerLayout;
	}

	/**
	 * Set the headerPosition
	 *
	 * @param string $headerPosition
	 */
	public function setHeaderPosition($headerPosition)
	{
	    $this->headerPosition = $headerPosition;
	}

	/**
	 * Get the headerPosition
	 *
	 * @return string
	 */
	public function getHeaderPosition()
	{
	    return $this->headerPosition;
	}

	/**
	 * Set the date
	 *
	 * @param int $date
	 */
	public function setDate($date)
	{
	    $this->date = $date;
	}

	/**
	 * Get the date
	 *
	 * @return int
	 */
	public function getDate()
	{
	    return $this->date;
	}

	/**
	 * Set the headerLink
	 *
	 * @param string $headerLink
	 */
	public function setHeaderLink($headerLink)
	{
	    $this->headerLink = $headerLink;
	}

	/**
	 * Get the headerLink
	 *
	 * @return string
	 */
	public function getHeaderLink()
	{
	    return $this->headerLink;
	}

	/**
	 * Set the subheader
	 *
	 * @param string $subheader
	 */
	public function setSubheader($subheader)
	{
	    $this->subheader = $subheader;
	}

	/**
	 * Get the subheader
	 *
	 * @return string
	 */
	public function getSubheader()
	{
	    return $this->subheader;
	}

	/**
	 * Set the bodytext
	 *
	 * @param string $bodytext
	 */
	public function setBodytext($bodytext)
	{
	    $this->bodytext = $bodytext;
	}

	/**
	 * Get the bodytext
	 *
	 * @return string
	 */
	public function getBodytext()
	{
	    return $this->bodytext;
	}

	/**
	 * Set the assets
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Asset> $assets
	 */
	public function setAssets($assets)
	{
	    $this->assets = $assets;
	}

	/**
	 * Get the assets
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Asset>
	 */
	public function getAssets()
	{
	    return $this->assets;
	}

	/**
	 * Set the imagewidth
	 *
	 * @param int $imagewidth
	 */
	public function setImagewidth($imagewidth)
	{
	    $this->imagewidth = $imagewidth;
	}

	/**
	 * Get the imagewidth
	 *
	 * @return int
	 */
	public function getImagewidth()
	{
	    return $this->imagewidth;
	}

	/**
	 * Set the imageheight
	 *
	 * @param int $imageheight
	 */
	public function setImageheight($imageheight)
	{
	    $this->imageheight = $imageheight;
	}

	/**
	 * Get the imageheight
	 *
	 * @return int
	 */
	public function getImageheight()
	{
	    return $this->imageheight;
	}

	/**
	 * Set the imageborder
	 *
	 * @param bool $imageborder
	 */
	public function setImageborder($imageborder)
	{
	    $this->imageborder = $imageborder;
	}

	/**
	 * Get the imageborder
	 *
	 * @return bool
	 */
	public function getImageborder()
	{
	    return $this->imageborder;
	}

	/**
	 * Set the imageorient
	 *
	 * @param int $imageorient
	 */
	public function setImageorient($imageorient)
	{
	    $this->imageorient = $imageorient;
	}

	/**
	 * Get the imageorient
	 *
	 * @return int
	 */
	public function getImageorient()
	{
	    return $this->imageorient;
	}

	/**
	 * Set the imagecols
	 *
	 * @param int $imagecols
	 */
	public function setImagecols($imagecols)
	{
	    $this->imagecols = $imagecols;
	}

	/**
	 * Get the imagecols
	 *
	 * @return int
	 */
	public function getImagecols()
	{
	    return $this->imagecols;
	}

	/**
	 * Set the imageZoom
	 *
	 * @param bool $imageZoom
	 */
	public function setImageZoom($imageZoom)
	{
	    $this->imageZoom = $imageZoom;
	}

	/**
	 * Get the imageZoom
	 *
	 * @return bool
	 */
	public function getImageZoom()
	{
	    return $this->imageZoom;
	}

	/**
	 * Set the layout
	 *
	 * @param int $layout
	 */
	public function setLayout($layout)
	{
	    $this->layout = $layout;
	}

	/**
	 * Get the layout
	 *
	 * @return int
	 */
	public function getLayout()
	{
	    return $this->layout;
	}

	/**
	 * Set the frameClass
	 *
	 * @param string $frameClass
	 */
	public function setFrameClass($frameClass)
	{
	    $this->frameClass = $frameClass;
	}

	/**
	 * Get the frameClass
	 *
	 * @return string
	 */
	public function getFrameClass()
	{
	    return $this->frameClass;
	}

	/**
	 * Set the spaceBeforeClass
	 *
	 * @param string $spaceBeforeClass
	 */
	public function setSpaceBeforeClass($spaceBeforeClass)
	{
	    $this->spaceBeforeClass = $spaceBeforeClass;
	}

	/**
	 * Get the spaceBeforeClass
	 *
	 * @return string
	 */
	public function getSpaceBeforeClass()
	{
	    return $this->spaceBeforeClass;
	}

	/**
	 * Set the spaceAfterClass
	 *
	 * @param string $spaceAfterClass
	 */
	public function setSpaceAfterClass($spaceAfterClass)
	{
	    $this->spaceAfterClass = $spaceAfterClass;
	}

	/**
	 * Get the spaceAfterClass
	 *
	 * @return string
	 */
	public function getSpaceAfterClass()
	{
	    return $this->spaceAfterClass;
	}

	/**
	 * Set the sectionIndex
	 *
	 * @param bool $sectionIndex
	 */
	public function setSectionIndex($sectionIndex)
	{
	    $this->sectionIndex = $sectionIndex;
	}

	/**
	 * Get the sectionIndex
	 *
	 * @return bool
	 */
	public function getSectionIndex()
	{
	    return $this->sectionIndex;
	}

	/**
	 * Set the linkToTop
	 *
	 * @param bool $linkToTop
	 */
	public function setLinkToTop($linkToTop)
	{
	    $this->linkToTop = $linkToTop;
	}

	/**
	 * Get the linkToTop
	 *
	 * @return bool
	 */
	public function getLinkToTop()
	{
	    return $this->linkToTop;
	}

	/**
	 * Set the sysLanguageUid
	 *
	 * @param int $sysLanguageUid
	 */
	public function setSysLanguageUid($sysLanguageUid)
	{
	    $this->sysLanguageUid = $sysLanguageUid;
	}

	/**
	 * Get the sysLanguageUid
	 *
	 * @return int
	 */
	public function getSysLanguageUid()
	{
	    return $this->sysLanguageUid;
	}

	/**
	 * Set the hidden
	 *
	 * @param bool $hidden
	 */
	public function setHidden($hidden)
	{
	    $this->hidden = $hidden;
	}

	/**
	 * Get the hidden
	 *
	 * @return bool
	 */
	public function getHidden()
	{
	    return $this->hidden;
	}

	/**
	 * Set the starttime
	 *
	 * @param int $starttime
	 */
	public function setStarttime($starttime)
	{
	    $this->starttime = $starttime;
	}

	/**
	 * Get the starttime
	 *
	 * @return int
	 */
	public function getStarttime()
	{
	    return $this->starttime;
	}

	/**
	 * Set the endtime
	 *
	 * @param int $endtime
	 */
	public function setEndtime($endtime)
	{
	    $this->endtime = $endtime;
	}

	/**
	 * Get the endtime
	 *
	 * @return int
	 */
	public function getEndtime()
	{
	    return $this->endtime;
	}

	/**
	 * Set the feGroup
	 *
	 * @param string $feGroup
	 */
	public function setFeGroup($feGroup)
	{
	    $this->feGroup = $feGroup;
	}

	/**
	 * Get the feGroup
	 *
	 * @return string
	 */
	public function getFeGroup()
	{
	    return $this->feGroup;
	}

	/**
	 * Set the editlock
	 *
	 * @param bool $editlock
	 */
	public function setEditlock($editlock)
	{
	    $this->editlock = $editlock;
	}

	/**
	 * Get the editlock
	 *
	 * @return bool
	 */
	public function getEditlock()
	{
	    return $this->editlock;
	}

	/**
	 * Set the categories
	 *
	 * @param int $categories
	 */
	public function setCategories($categories)
	{
	    $this->categories = $categories;
	}

	/**
	 * Get the categories
	 *
	 * @return int
	 */
	public function getCategories()
	{
	    return $this->categories;
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}