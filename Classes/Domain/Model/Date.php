<?php
namespace Maagit\Maagitevent\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Model
	class:				Date

	description:		Model for the event date.
						Inherits the datas of added event dates in tt_content

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Date extends \Maagit\Maagitevent\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var int
     */
    protected ?int $uid;

	/**
	 * @var int
     */
    protected ?int $pid;

	/**
	 * @var int
     */
    protected $fromdate;

	/**
	 * @var int
     */
    protected $todate;

	/**
	 * @var int
     */
    protected $individuals;

	/**
	 * @var string
     */
    protected $repeat;

	/**
	 * @var int
     */
    protected $every;

	/**
	 * @var string
     */
    protected $interval;

	/**
     * @var bool
     */
    protected $monday;

	/**
     * @var bool
     */
    protected $tuesday;

	/**
     * @var bool
     */
    protected $wednesday;

	/**
	 * @var bool
     */
    protected $thursday;

	/**
     * @var bool
     */
    protected $friday;

	/**
     * @var bool
     */
    protected $saturday;

	/**
     * @var bool
     */
    protected $sunday;

	/**
     * @var bool
     */
    protected $last;

    /**
     * @var string
     */
    protected $location;

	/**
	 * @var double
     */
    protected $price;
	
	/**
	 * @var bool
     */
    protected $cancelled;

	/**
	 * @var bool
     */
    protected $full;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
	 * Set the uid
	 *
	 * @param int $uid
	 */
	public function setUid($uid)
	{
	    $this->uid = $uid;
	}

	/**
	 * Set the fromdate
	 *
	 * @param int $fromdate
	 */
	public function setFromdate($fromdate)
	{
	    $this->fromdate = $fromdate;
	}

	/**
	 * Get the fromdate
	 *
	 * @return int
	 */
	public function getFromdate()
	{
		return $this->fromdate;
	}

	/**
	 * Get the datefrom
	 *
	 * @return int
	 */
	public function getDatefrom()
	{
	    $dateService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateService');
		return $dateService->getDateTime($this->fromdate);
	}

	/**
	 * Set the todate
	 *
	 * @param int $todate
	 */
	public function setTodate($todate)
	{
	    $this->todate = $todate;
	}

	/**
	 * Get the todate
	 *
	 * @return int
	 */
	public function getTodate()
	{
		if (empty($this->todate) || $this->todate == 0)
		{
			return $this->getFromdate();
		}
		return $this->todate;
	}

	/**
	 * Get the dateto
	 *
	 * @return int
	 */
	public function getDateto()
	{
		if (empty($this->todate) || $this->todate == 0)
		{
			return $this->getFromdate();
		}
		$dateService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateService');
		return $dateService->getDateTime($this->todate);
	}

	/**
	 * Set the individuals
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Individual> $individuals
	 */
	public function setIndividuals($individuals)
	{
	    $this->individuals = $individuals;
	}

	/**
	 * Get the individuals
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitevent\Domain\Model\Individual>
	 */
	public function getIndividuals()
	{
	    return $this->individuals;
	}

	/**
	 * Set the repeat
	 *
	 * @param string $repeat
	 */
	public function setRepeat($repeat)
	{
	    if ($repeat == null) {$repeat = '';}
		$this->repeat = $repeat;
	}

	/**
	 * Get the repeat
	 *
	 * @return string
	 */
	public function getRepeat()
	{
		return $this->repeat;
	}

	/**
	 * Set the every
	 *
	 * @param int $every
	 */
	public function setEvery($every)
	{
	    $this->every = $every;
	}

	/**
	 * Get the every
	 *
	 * @return int
	 */
	public function getEvery()
	{
	    return $this->every;
	}

	/**
	 * Set the interval
	 *
	 * @param string $interval
	 */
	public function setInterval($interval)
	{
	    if ($interval == null) {$interval = '';}
		$this->interval = $interval;
	}

	/**
	 * Get the interval
	 *
	 * @return string
	 */
	public function getInterval()
	{
	    return $this->interval;
	}

	/**
	 * Set the monday
	 *
	 * @param bool $monday
	 */
	public function setMonday($monday)
	{
	    $this->monday = $monday;
	}

	/**
	 * Get the monday
	 *
	 * @return bool
	 */
	public function getMonday()
	{
	    return $this->monday;
	}

	/**
	 * Set the tuesday
	 *
	 * @param bool $tuesday
	 */
	public function setTuesday($tuesday)
	{
	    $this->tuesday = $tuesday;
	}

	/**
	 * Get the tuesday
	 *
	 * @return bool
	 */
	public function getTuesday()
	{
	    return $this->tuesday;
	}

	/**
	 * Set the wednesday
	 *
	 * @param bool $wednesday
	 */
	public function setWednesday($wednesday)
	{
	    $this->wednesday = $wednesday;
	}

	/**
	 * Get the wednesday
	 *
	 * @return bool
	 */
	public function getWednesday()
	{
	    return $this->wednesday;
	}

	/**
	 * Set the thursday
	 *
	 * @param bool $thursday
	 */
	public function setThursday($thursday)
	{
	    $this->thursday = $thursday;
	}

	/**
	 * Get the thursday
	 *
	 * @return bool
	 */
	public function getThursday()
	{
	    return $this->thursday;
	}

	/**
	 * Set the friday
	 *
	 * @param bool $friday
	 */
	public function setFriday($friday)
	{
	    $this->friday = $friday;
	}

	/**
	 * Get the friday
	 *
	 * @return bool
	 */
	public function getFriday()
	{
	    return $this->friday;
	}

	/**
	 * Set the saturday
	 *
	 * @param bool $saturday
	 */
	public function setSaturday($saturday)
	{
	    $this->saturday = $saturday;
	}

	/**
	 * Get the saturday
	 *
	 * @return bool
	 */
	public function getSaturday()
	{
	    return $this->saturday;
	}

	/**
	 * Set the sunday
	 *
	 * @param bool $sunday
	 */
	public function setSunday($sunday)
	{
	    $this->sunday = $sunday;
	}

	/**
	 * Get the sunday
	 *
	 * @return bool
	 */
	public function getSunday()
	{
	    return $this->sunday;
	}

	/**
	 * Set the last
	 *
	 * @param bool $last
	 */
	public function setLast($last)
	{
	    $this->last = $last;
	}

	/**
	 * Get the last
	 *
	 * @return bool
	 */
	public function getLast()
	{
	    return $this->last;
	}

	/**
	 * Set the location
	 *
	 * @param string $location
	 */
	public function setLocation($location)
	{
	    $this->location = $location;
	}

	/**
	 * Get the location
	 *
	 * @return string
	 */
	public function getLocation()
	{
	    return $this->location;
	}

	/**
	 * Set the price
	 *
	 * @param double $price
	 */
	public function setPrice($price)
	{
	    $this->price = $price;
	}

	/**
	 * Get the price
	 *
	 * @return double
	 */
	public function getPrice()
	{
	    return $this->price;
	}

	/**
	 * Set the cancelled
	 *
	 * @param bool $cancelled
	 */
	public function setCancelled($cancelled)
	{
	    $this->cancelled = $cancelled;
	}

	/**
	 * Get the cancelled
	 *
	 * @return bool
	 */
	public function getCancelled()
	{
	    return $this->cancelled;
	}
	
	/**
	 * Set the full
	 *
	 * @param bool $full
	 */
	public function setFull($full)
	{
	    $this->full = $full;
	}

	/**
	 * Get the full
	 *
	 * @return bool
	 */
	public function getFull()
	{
	    return $this->full;
	}

	/**
	 * Get the isRepeating
	 *
	 * @return bool
	 */
	public function getIsRepeating()
	{
	    $isRepeating = false;
		if ($this->getRepeat() != '' && $this->getInterval() != '')
		{
			$isRepeating = true;
		}
		if ($this->getRepeat() == 'every' && $this->getEvery() == 0)
		{
			$isRepeating = false;
		}
		return $isRepeating;
	}
	
	/**
	 * Get the weekdays
	 *
	 * @return array
	 */
	public function getWeekdays()
	{
	    $weekdays = array();
		if ($this->getMonday())
		{
			$weekdays[] = 0;
		}
		if ($this->getTuesday())
		{
			$weekdays[] = 1;
		}
		if ($this->getWednesday())
		{
			$weekdays[] = 2;
		}
		if ($this->getThursday())
		{
			$weekdays[] = 3;
		}
		if ($this->getFriday())
		{
			$weekdays[] = 4;
		}
		if ($this->getSaturday())
		{
			$weekdays[] = 5;
		}
		if ($this->getSunday())
		{
			$weekdays[] = 6;
		}
		return $weekdays;
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}