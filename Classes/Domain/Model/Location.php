<?php
namespace Maagit\Maagitevent\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Model
	class:				Location

	description:		Model for the location.
						Model to group event dates by locations.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Location extends \Maagit\Maagitevent\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */

	/**
	 * @var int
     */
    protected ?int $uid;

	/**
	 * @var int
     */
    protected ?int $pid;

	/**
	 * @var string
     */
    protected $location;

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DateTime>
     */
    protected $dates;	


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		$this->dates = $this->makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage');
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
	 * Set the uid
	 *
	 * @param int $uid
	 */
	public function setUid($uid)
	{
	    $this->uid = $uid;
	}

	/**
	 * Set the location
	 *
	 * @param string $location
	 */
	public function setLocation($location)
	{
	    $this->location = $location;
	}

	/**
	 * Get the location
	 *
	 * @return string
	 */
	public function getLocation()
	{
		return $this->location;
	}

	/**
	 * set the dates
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DateTime>
	 */
	public function setDates($dates)
	{
		$this->dates = $dates;
	}

	/**
	 * Get the dates
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DateTime>
	 */
	public function getDates()
	{
		return $this->dates;
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}