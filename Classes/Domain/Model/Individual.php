<?php
namespace Maagit\Maagitevent\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Model
	class:				Individual

	description:		Model for the individual date.
						Inherits the datas of added individual dates in tt_content

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Individual extends \Maagit\Maagitevent\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitevent\Service\Date\DateService
     */
    protected $dateService;

	/**
	 * @var int
     */
    protected ?int $uid;

	/**
	 * @var int
     */
    protected ?int $pid;

	/**
	 * @var int
     */
    protected $individualdatefrom;

	/**
	 * @var int
     */
    protected $individualdateto;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{

	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
	 * Set the uid
	 *
	 * @param int $uid
	 */
	public function setUid($uid)
	{
	    $this->uid = $uid;
	}

	/**
	 * Set the individualdatefrom
	 *
	 * @param int $individualdatefrom
	 */
	public function setIndividualdatefrom($individualdatefrom)
	{
	    $this->individualdatefrom = $individualdatefrom;
	}

	/**
	 * Get the individualdatefrom
	 *
	 * @return int
	 */
	public function getIndividualdatefrom()
	{
		return $this->individualdatefrom;
	}
	
	/**
	 * Get the datefrom
	 *
	 * @return \DateTime
	 */
	public function getDatefrom()
	{
		$dateService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateService');
		return $dateService->getDateTime($this->getIndividualdatefrom());
	}

	/**
	 * Set the individualdateto
	 *
	 * @param int $individualdateto
	 */
	public function setIndividualdateto($individualdateto)
	{
		$this->individualdateto = $individualdateto;
	}

	/**
	 * Get the individualdateto
	 *
	 * @return int
	 */
	public function getIndividualdateto()
	{
		if (empty($this->individualdateto) || $this->individualdateto == 0)
		{
			return $this->getIndividualdatefrom();
		}
	    return $this->individualdateto;
	}

	/**
	 * Get the dateto
	 *
	 * @return \DateTime
	 */
	public function getDateto()
	{
		$dateService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateService');
		return $dateService->getDateTime($this->getIndividualdateto());
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}