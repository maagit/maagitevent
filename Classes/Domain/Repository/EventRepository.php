<?php
namespace Maagit\Maagitevent\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Repository
	class:				EventRepository

	description:		Repository for the "event" model.
						Inherits the method "create" for creating fitting models.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version
						2021-09-19	Urs Maag		ObjectManager removed
						2022-10-10	Urs Maag		Version 12.0.0 compatibility
													- remove "$this->objectManager"

------------------------------------------------------------------------------------- */


class EventRepository extends \Maagit\Maagitevent\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitevent\Domain\Repository\AssetRepository
     */
	protected $assetRepository;
	
	/**
	 * @var \Maagit\Maagitevent\Domain\Repository\DateRepository
     */
	protected $dateRepository;
	
	/**
	 * @var \Maagit\Maagitevent\Domain\Repository\ExceptRepository
     */
	protected $exceptRepository;

	/**
	 * @var \Maagit\Maagitevent\Domain\Repository\LocationRepository
     */
	protected $locationRepository;	

	/**
	 * @var \Maagit\Maagitevent\Service\Date\DateService
     */
	protected $dateService;

	/**
	 * @var array
     */
	protected $defaultOrderings = [
        'crdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
        'uid' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
    ];


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Initialize the object, set default values
     *
     */
	public function initializeObject() {
		// make initalization things and inject repositories and services
		parent::initializeObject();
		$this->assetRepository = $this->makeInstance('Maagit\\Maagitevent\\Domain\\Repository\\AssetRepository');
		$this->dateRepository = $this->makeInstance('Maagit\\Maagitevent\\Domain\\Repository\\DateRepository');
		$this->exceptRepository = $this->makeInstance('Maagit\\Maagitevent\\Domain\\Repository\\ExceptRepository');
		$this->locationRepository = $this->makeInstance('Maagit\\Maagitevent\\Domain\\Repository\\LocationRepository');
		$this->dateService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateService');

		// make default query settings
		$querySettings = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		if (empty($this->configuration['persistence']['storagePid']))
		{
			$querySettings->setRespectStoragePage(false);
		}
		else
		{
			$pageRepository = $this->makeInstance('Maagit\\Maagitevent\\Domain\\Repository\\PageRepository');
			$pidList = $pageRepository->getPageTreeUids(
				\TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $this->configuration['persistence']['storagePid'], true),
				(!empty($this->configuration['persistence']['recursive'])) ? $this->configuration['persistence']['recursive'] : 0
			);
			$querySettings->setRespectStoragePage(true);
			$querySettings->setStoragePageIds($pidList);
		}
        $this->setDefaultQuerySettings($querySettings);
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Create a new object
     *
	 * @param	$arguments		array										variable arguments
	 * @return 					\Maagit\Maagitevent\Domain\Model\Base		domain model object
     */
	public function create(...$arguments)
	{
		$eventObj = parent::create();
		$record = $arguments[0];
		$propertyService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Property\\PropertyService');
		$eventObj = $propertyService->setProperties($eventObj, $record);
		$eventObj->setAssets($this->assetRepository->findByUid($record['uid']));
		$eventObj->setDates($this->dateRepository->findByUid($record['uid']));
		$eventObj->setExcepts($this->exceptRepository->findByUid($record['uid']));
		$eventObj->setLocations($this->locationRepository->findByEvent($eventObj));
		return $eventObj;
	}

	/**
     * get all events
	 *
	 * @param	-
	 * @return	array						the events
     */
	public function findAll()
	{
		$events = array();
		foreach ($this->getFromDatabase() as $object)
		{
			$events = $this->divHelper->arrayMerge($this->getUniqueEvents($object), $events);
		}
		return $events;
	}

	/**
     * get events by event object
	 *
	 * @param	\Maagit\Maagitevent\Domain\Model\Event		the event object
	 * @return	array										the events
     */
	public function findByEvent(\Maagit\Maagitevent\Domain\Model\Event $event)
	{
		return $this->getUniqueEvents($event);
	}

	/**
     * get event by internal id
	 *
	 * @param	string										$id				the internal event id
	 * @return	\Maagit\Maagitevent\Domain\Model\Event						the event object
     */
	public function findById(string $id)
	{
		foreach ($this->findAll() as $event)
		{
			if ($event->getId() == $id)
			{
				return $event;
			}
		}
		return null;
	}
	
	/**
     * get event id by slug
	 *
	 * this function are calling from the routeEnhancer for getting speaking urls
	 * and therefore is not in "normal" process and must make initialize things
	 * itself
	 *
	 * @param	string										$slug			the slug
	 * @param	int											$pluginPid		the uid of page with event plugin
	 *																		- for reading correct settings
	 * @return	string														the event id
     */
	public function findBySlug(string $slug, int $pluginPid)
	{
		// initialize helper, repository and plugin settings
		$this->settings = array();
		$this->configuration = array();
		$this->getFlexformSettings($pluginPid);
		$slugHelper = $this->makeInstance('TYPO3\\CMS\\Core\\DataHandling\\SlugHelper', '', '', array());
		$this->initializeObject();

		// loop events and compare title/date
		$fragments = explode('/', $slug);
		foreach ($this->findAll() as $event)
		{
			if ($slugHelper->sanitize($event->getTitle()) == $fragments[0] && $this->dateService->getDateTimeStringValue($event->getDatefrom(), 'YYYY-MM-dd') == $fragments[1])
			{
				return $event->getId();
			}
		}
		return null;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * get events from database
	 *
	 * @param	-
	 * @return	array								the event objects from database
     */
	protected function getFromDatabase()
	{
		$dbEvents = array();
		$records = $this->createQuery()->execute(true);
		foreach ($records as $record)
		{
			$dbEvents[] = $this->create($record);
		}
		return $dbEvents;
	}

	/**
     * get unique event objects
	 *
	 * @param	\Maagit\Maagitevent\Domain\Model\Event			$event			the event
	 * @return	array															the unique event objects
     */
	protected function getUniqueEvents(\Maagit\Maagitevent\Domain\Model\Event $event)
	{
		$events = array();
		// loop dates and create unique events
		foreach ($event->getDates() as $date)
		{
			if ($date->getIsRepeating())
			{
				$events = $this->divHelper->arrayMerge($this->getRepeatedEvents($event, $date), $events);
			}
			else
			{
				$events = $this->divHelper->arrayMerge($this->getUniqueEvent($event, $date), $events);
			}
		}


		// create unique internal id of each event
		$index = 0;
		foreach ($events as $event)
		{
			$id = $event->getUid().'_'.$index;
			$event->setId($id);
			$index++;
		}


		// short the event list, if the "next" field is set
		if (!$this->settings['dontRespectNext'] && $event->getNext() > 0)
		{
			$sortingService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Property\\SortingService');
			$events = $sortingService->sort($events, array('getDatefrom' => 'asc'));
			$events = array_slice($events, 0, $event->getNext(), true);
		}
		return $events;
	}
	
	/**
     * make a unique event object
	 *
	 * @param	\Maagit\Maagitevent\Domain\Model\Event				$event			the event
	 * @param	\Maagit\Maagitevent\Domain\Model\Date				$date			the event date object
	 * @return	\Maagit\Maagitevent\Domain\Model\Event | NULL						the unique event object or null
     */
	protected function getUniqueEvent(\Maagit\Maagitevent\Domain\Model\Event $event, \Maagit\Maagitevent\Domain\Model\Date $date)
	{
		if (!$this->dateService->isInExceptRange($this->dateService->getDateTime($date->getFromdate()), $this->dateService->getDateTime($date->getTodate()), $this->dateService->getExceptRanges($event)))
		{
			return $this->cloneEvent($event, $date);
		}
		return null;
	}

	/**
     * get unique event object on events, which have repeating settings
	 *
	 * @param	\Maagit\Maagitevent\Domain\Model\Event			$event			the event object
	 * @param	\Maagit\Maagitevent\Domain\Model\Date			$date			the event date object
	 * @return	array															the repeated event objects
     */
	protected function getRepeatedEvents(\Maagit\Maagitevent\Domain\Model\Event $event, \Maagit\Maagitevent\Domain\Model\Date $date)
	{
		// get all relevant repeating dates
		$repeatingDates = $this->dateService->getRepeatingDates(
			$this->dateService->getDateTime($date->getFromdate()),
			$this->dateService->getDateTime($date->getTodate()),
			$date->getEvery(),
			$date->getInterval(),
			$date->getWeekdays(),
			$date->getLast()
		);

		// check on except dates
		$events = array();
		foreach ($repeatingDates as $repeatingDate)
		{
			if (!$this->dateService->isInExceptRange($repeatingDate, $repeatingDate, $this->dateService->getExceptRanges($event)))
			{
				$newEvent = $this->cloneEvent($event, $date);
				$newEvent->setDatefrom($repeatingDate);
				$newEvent->setDateto($this->dateService->addTime($repeatingDate, $this->dateService->getDateTime($date->getTodate())));
				$events = $this->divHelper->arrayMerge($newEvent, $events);
			}
		}

		// return added events
		return $events;
	}

	/**
     * clone a event
	 *
	 * @param	\Maagit\Maagitevent\Domain\Model\Event			$event			the event object
	 * @param	\Maagit\Maagitevent\Domain\Model\Date			$date			the event date object
	 * @return	\Maagit\Maagitevent\Domain\Model\Event							the cloned event object
     */
	protected function cloneEvent(\Maagit\Maagitevent\Domain\Model\Event $event, \Maagit\Maagitevent\Domain\Model\Date $date)
	{
		$clonedEvent = clone $event;
		$clonedEvent->setDatefrom($this->dateService->getDateTime($date->getFromdate()));
		$clonedEvent->setDateto($this->dateService->getDateTime($date->getTodate()));
		$clonedEvent->setIndividuals($date->getIndividuals());
		$clonedEvent->setPrice(($date->getPrice()==0) ? $clonedEvent->getPrice() : $date->getPrice());
		$clonedEvent->setLocation(($date->getLocation()=='') ? $clonedEvent->getLocation() : $date->getLocation());
		$clonedEvent->setCancelled(($date->getCancelled()=='') ? $clonedEvent->getCancelled() : $date->getCancelled());
		$clonedEvent->setFull(($date->getFull()=='') ? $clonedEvent->getFull() : $date->getFull());
		return $clonedEvent;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
