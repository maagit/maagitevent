<?php
namespace Maagit\Maagitevent\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Repository
	class:				AssetRepository

	description:		Repository for the "asset" model.
						Inherits the method "create" for creating fitting models.
						Inherits the method "findByUid" to select the asset of given
						event.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class AssetRepository extends \Maagit\Maagitevent\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Create a new object
     *
	 * @param	$arguments		array										variable arguments
	 * @return 					\Maagit\Maagitevent\Domain\Model\Base		domain model object
     */
	public function create(...$arguments)
	{
		$asset = parent::create();
		$fileObject = $arguments[0];
		$asset->setUid($fileObject->getUid());
		$asset->setPathAndFilename($fileObject->getPublicUrl());
		$asset->setPath(substr($fileObject->getPublicUrl(), 0, strrpos($fileObject->getPublicUrl(), $fileObject->getName())));
		$asset->setFilename($fileObject->getName());
		$asset->setAlternative($fileObject->getAlternative());
		$asset->setDescription($fileObject->getDescription());
		$asset->setLink($fileObject->getLink());
		$asset->setTitle($fileObject->getTitle());
		$asset->setCrop($fileObject->getProperty('crop'));
		return $asset;
	}

	/**
     * Select assets from tt_content by given uid.
     *
     * @param 	int												$uid        the uid of the tt_content element
	 * @return 	TYPO3\CMS\Extbase\Persistence\ObjectStorage					object storage with file references
     */
	public function findByUid($uid)
	{
		// get assets by relation
		$fileRepository = $this->makeInstance('TYPO3\\CMS\\Core\\Resource\\FileRepository');
		$fileObjects = $fileRepository->findByRelation('tt_content', 'assets', $uid);

		// make asset storage
		$assets = $this->makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage');
		foreach ($fileObjects as $fileObject)
		{
			$assets->attach($this->create($fileObject));
		}

		// return asset object storage
		return $assets;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}