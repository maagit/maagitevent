<?php
namespace Maagit\Maagitevent\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Repository
	class:				DateRepository

	description:		Repository for the "date" model.
						Inherits the method "create" for creating fitting models.
						Inherits the method "findByTtContent" to select the dates of given
						event.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version
						2021-09-19	Urs Maag		ObjectManager removed
						2022-10-10	Urs Maag		Version 12.0.0 compatibility
													- remove "$this->objectManager"

------------------------------------------------------------------------------------- */


class DateRepository extends \Maagit\Maagitevent\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitevent\Domain\Repository\IndividualRepository
     */
	protected $individualRepository;

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Initialize the object, set default values
     *
     */
	public function initializeObject() {
		parent::initializeObject();
		$this->individualRepository = $this->makeInstance('Maagit\\Maagitevent\\Domain\\Repository\\IndividualRepository');
		$querySettings = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		$querySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($querySettings);
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Create a new object
     *
	 * @param	$arguments		array										variable arguments
	 * @return 					\Maagit\Maagitevent\Domain\Model\Base		domain model object
     */
	public function create(...$arguments)
	{
		$dateObj = parent::create();
		$record = $arguments[0];
		$propertyService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Property\\PropertyService');
		$dateObj = $propertyService->setProperties($dateObj, $record);
		$dateObj->setIndividuals($this->individualRepository->findByUid($record['uid']));
		return $dateObj;
	}

	/**
     * get all given dates from database
	 *
	 * @param	int													$uid		the tt_content uid
	 * @return	\TYPO3\CMS\Extbase\Persistence\ObjectStorage					the dates
     */
	public function findByUid($uid)
	{
		$dates = $this->makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage');
		$objects = $this->findByTtContent($uid);
		foreach ($objects as $object)
		{
			$dates->attach($this->create($object->toArray()));
		}
		return $dates;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}