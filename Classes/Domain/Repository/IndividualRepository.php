<?php
namespace Maagit\Maagitevent\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Repository
	class:				IndividualRepository

	description:		Repository for the "individual" model.
						Inherits the method "create" for creating fitting models.
						Inherits the method "findBy..." to select the individuals
						of given event.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version
						2021-09-19	Urs Maag		ObjectManager removed

------------------------------------------------------------------------------------- */


class IndividualRepository extends \Maagit\Maagitevent\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Initialize the object, set default values
     *
     */
	public function initializeObject() {
		parent::initializeObject();
		$querySettings = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		$querySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($querySettings);
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Create a new object
     *
	 * @param	$arguments		array										variable arguments
	 * @return 					\Maagit\Maagitevent\Domain\Model\Base		domain model object
     */
	public function create(...$arguments)
	{
		$individualObj = parent::create();
		$record = $arguments[0];
		$propertyService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Property\\PropertyService');
		$individualObj = $propertyService->setProperties($individualObj, $record);
		return $individualObj;
	}

	/**
     * get all given individuals from database
	 *
	 * @param	int													$uid		the tt_content uid
	 * @return	\TYPO3\CMS\Extbase\Persistence\ObjectStorage					the excepts
     */
	public function findByUid($uid)
	{
		$individuals = $this->makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage');
		$objects = $this->findByTxMaagiteventDomainModelDate($uid);
		foreach ($objects as $object)
		{
			$individuals->attach($this->create($object->toArray()));
		}
		return $individuals;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}