<?php
namespace Maagit\Maagitevent\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Repository
	class:				LocationRepository

	description:		Repository for the "location" model.
						Inherits the method "create" for creating fitting models.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class LocationRepository extends \Maagit\Maagitevent\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitevent\Service\Date\DateService
     */
	protected $dateService;

	/**
	 * @var \Maagit\Maagitevent\Service\Property\SortingService
     */
	protected $sortingService;

	/**
	 * @var array
     */
	protected $locations;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		$this->dateService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Date\\DateService');
		$this->sortingService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Property\\SortingService');
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * get all locations, declarated in given event
	 *
	 * @param	\Maagit\Maagitevent\Domain\Model\Event					$event		the event object
	 * @return	\TYPO3\CMS\Extbase\Persistence\ObjectStorage						the locations
     */
	public function findByEvent(\Maagit\Maagitevent\Domain\Model\Event $event)
	{
		$this->locations = $event->getLocations();
		foreach ($event->getDates() as $date)
		{
			if (!empty($date->getLocation()))
			{
				if ($date->getIsRepeating())
				{
					$repeatingDates = $this->dateService->getRepeatingDates(
						$this->dateService->getDateTime($date->getFromdate()),
						$this->dateService->getDateTime($date->getTodate()),
						$date->getEvery(),
						$date->getInterval(),
						$date->getWeekdays(),
						$date->getLast()
					);
					foreach ($repeatingDates as $repeatingDate)
					{
						if (!$this->dateService->isInExceptRange($repeatingDate, $repeatingDate, $this->dateService->getExceptRanges($event)))
						{
							$this->addLocationDate($repeatingDate, $date->getLocation());
						}
					}
				}
				else
				{
					if (!$this->dateService->isInExceptRange($this->dateService->getDateTime($date->getFromdate()), $this->dateService->getDateTime($date->getTodate()), $this->dateService->getExceptRanges($event)))
					{
						$this->addLocationDate($this->dateService->getDateTime($date->getFromdate()), $date->getLocation());
					}
				}
			}
		}
		return $this->locations;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * get location object by location name
	 *
	 * @param	string												$locationName		the location name
	 * @return	\Maagit\Maagitevent\Domain\Model\Location								the location object
     */
	protected function findByLocation(string $locationName)
	{
		foreach ($this->locations as $location)
		{
			if ($location->getLocation() == $locationName)
			{
				return $location;
			}
		}
		return false;
	}

	/**
     * add a date to location
	 *
	 * @param	\DateTime				$date				the date
	 * @param	string					$locationName		the location name
	 * @return	void
     */
	protected function addLocationDate(\DateTime $date, string $locationName)
	{
		$locationObject = $this->findByLocation($locationName);
		if (!$locationObject)
		{
			$locationObject = $this->create();
			$locationObject->setLocation($locationName);
			$dates = $locationObject->getDates();
			$dates->attach($date);
			$locationObject->setDates($dates);
			$this->locations->attach($locationObject);
		}
		else
		{
			$locations = $locationObject->getDates();
			$locations->attach($date);
			$locationObject->setDates($locations);
		}
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}