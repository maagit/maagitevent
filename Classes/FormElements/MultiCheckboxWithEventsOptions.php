<?php
namespace Maagit\Maagitevent\FormElements;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Form
	class:				MultiCheckboxWithEventsOptions

	description:		Get events from given pid and render a list form
						element.

	created:			2023-05-22
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-05-22	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\FrontendRestrictionContainer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Model\FormElements\GenericFormElement;
use TYPO3\CMS\Frontend\Category\Collection\CategoryCollection;

class MultiCheckboxWithEventsOptions extends GenericFormElement
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var array
     */
	protected $plugins = array(
		'maagitevent_list'
	);

	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager
     */
	protected $configurationManager;

	/**
	 * @var array
     */
    protected $settings;
	
	/**
	 * @var array
     */
    protected $configuration;
	
	/**
	 * @var int
     */
    protected $pluginId;

	/**
	 * @var string
     */
    protected $content;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * set property -> options for events
     *
     * @param	string				$key		the key of form element
	 * @param	mixed				$value		the value for this element
     * @return	void
     */
	public function setProperty(string $key, $value)
    {
		if ($key === 'eventsPluginId')
		{
            $this->pluginId = $value;
			// @extensionScannerIgnoreLine
			if (!empty($this->content))
			{
				// @extensionScannerIgnoreLine
				$this->setProperty('options', $this->getOptions($this->pluginId, $this->content));
			}
            return;
        }
		if ($key === 'eventsContent')
		{
            // @extensionScannerIgnoreLine
			$this->content = $value;
			if (!empty($this->pluginId))
			{
				// @extensionScannerIgnoreLine
				$this->setProperty('options', $this->getOptions($this->pluginId, $this->content));
			}
            return;
        }
        parent::setProperty($key, $value);
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * get options for events
     *
     * @param	int					$pluginId		the plugin id, which contains the events configuration
	 * @param	string				$content		the content to render
     * @return	array								the options
     */
	protected function getOptions(int $pluginId, string $content) : array
    {
        $options = array();
        foreach ($this->getEventsForPluginId($pluginId, $content) as $entry)
		{
            $options[$entry['fieldValue']] = $entry['fieldText'];
        }
        return $options;
    }

    /**
     * select entries from database
     *
     * @param	int					$pluginId		the plugin id, which contains the events configuration
	 * @param	string				$content		the content to render
     * @return	array								the selected rows
     */
    protected function getEventsForPluginId(int $pluginId, string $content) : array
    {
        $this->configurationManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
		$this->settings = array();
		$this->configuration = array();
		$this->getPluginSettings($pluginId);

		$eventRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitevent\\Domain\\Repository\\EventRepository');
		$eventRepository->setSettings($this->settings);
		$eventRepository->initializeObject();
		$events = $eventRepository->findAll();

		$sortingService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitevent\\Service\\Property\\SortingService');
		$sortingService->setSettings($this->settings);
		$events = $sortingService->sort($events);

		$renderTypolinkService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitevent\\Service\\Content\\RenderTypolinkService');
		$renderFontawesomeService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitevent\\Service\\Content\\RenderFontawesomeService');
		$renderPlaceholderService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitevent\\Service\\Content\\RenderPlaceholderService');

		$checkboxes = array();
		foreach ($events as $event)
		{
			$eventContent = $renderTypolinkService->renderTypolink($content);
			$eventContent = $renderFontawesomeService->renderFontawesome($eventContent);
			$eventContent = $renderPlaceholderService->renderPlaceholder($eventContent, $event);
			$checkboxes[] = array('fieldValue' => $event->getId(), 'fieldText' => $eventContent);
		}
		return $checkboxes;
    }


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
     * Read plugin flexform configuration
     *
	 * @param	int					$pluginId		the plugin id
	 * @return	void
     */
	private function getPluginSettings(int $pluginId)
	{
		$queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('tt_content');	
		$queryBuilder 
			->select('tt_content.*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->eq(
					'tt_content.uid', 
					$queryBuilder->createNamedParameter($pluginId, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
		);
		$records = $queryBuilder->executeQuery()->fetchAllAssociative();
		if (!$records)
		{
			throw new \Exception('Error on getting event plugin settings. Plugin with id '.$pluginId.' not found');
		}
		foreach ($records as $key => $data)
		{
			// settings
			$ffConf = $this->getFlexform2ConfigArray($data['pi_flexform']);
			if (!empty($ffConf['settings']))
			{
				$this->settings = array_replace_recursive($this->settings, $ffConf['settings']);	
			}
			
			// other than settings
			unset($ffConf['settings']);
			if (!empty($ffConf))
			{
				$this->configuration = array_replace_recursive($this->configuration, $ffConf);
			}
		}
	}
	
	/**
     * Convert given flexform value to array
     *
	 * @param	$xml						string				the pi_flexform as xml string
	 * @return								array				the flexform data as config array
     */
	private function getFlexform2ConfigArray($xml)
	{
		$flexFormTools = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Configuration\\FlexForm\\FlexFormTools');
		$flexformData = array();
		try
		{
			$xmlArray = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($xml);
			foreach ($xmlArray as $sheets)
			{
				foreach ($sheets as $elements)
				{
					foreach ($elements as $fields)
					{
						foreach ($fields as $key => $value)
						{
							$temp = $this->getFFValue($key, $value['vDEF'], array());
							$flexformData = array_merge_recursive($flexformData, $temp);
						}
					}
				}
			}
		}
		catch (\Exception $ex) { }
		return $flexformData;
	}

	/**
     * Get the flexform value as key => value pair.
	 * Recursive function, for exploding keys with subkeys in a array structure
     *
	 * @param	$key						string				the key from pi_flexform (can be key with subkey, e.g. settings.delivery.method)
	 * @param	$value						string				the value from pi_flexform
	 * @param	$array						array				the parent array for creating recursively array structure
	 * @param	$delimiter					string				the delimiter of "keys with subkeys", default '.'
	 * @param	$key						string				the key from pi_flexform (can be key with subkey, e.g. settings.delivery.method)
	 * @return								array				the flexform data as config array
     */
	private function getFFValue($key, $value, $array, $delimiter = '.')
	{
		if (strpos($key, $delimiter) === FALSE)
		{
			$array = array($key => $value);
			return $array;
		}
		else
		{
			$segment = substr($key, 0, strpos($key, $delimiter));
			$newKey = substr($key, strpos($key, $delimiter) + 1);
			$array = array($segment => $this->getFFValue($newKey, $value, $array));
			return $array;
		}
	}
}