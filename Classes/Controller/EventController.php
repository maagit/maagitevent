<?php
namespace Maagit\Maagitevent\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Controller
	class:				EventController

	description:		Main class for the event.
						Process the action "list".

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version
						2021-12-24	Urs Maag		PHP 8, fix non existing array keys
						2022-10-10	Urs Maag		Make Typo3 12.0.0 compatible
													- return "ForwardResponse" on actions
													- remove "objectManager"

------------------------------------------------------------------------------------- */


class EventController extends \Maagit\Maagitevent\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitevent\Domain\Repository\EventRepository
     */
    protected $eventRepository;
	
	/**
	 * @var \Maagit\Maagitevent\Service\Content\RenderService
     */
    protected $renderService;

	/**
	 * @var \Maagit\Maagitevent\Service\Property\SortingService
     */
    protected $sortingService;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// inject repositories and services
		$this->eventRepository = $this->makeInstance('Maagit\\Maagitevent\\Domain\\Repository\\EventRepository');
		$this->sortingService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Property\\SortingService');
		$this->renderService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Content\\RenderService');
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Action for this controller.
	 * route view.
     *
     * @return void
     */
    public function routeAction() : \Psr\Http\Message\ResponseInterface
    {
		return new \TYPO3\CMS\Extbase\Http\ForwardResponse($this->settings['route'], 'Event', 'Maagitevent', array());
    }

	/**
     * Action for this controller.
	 * list events.
     *
     * @return \Psr\Http\Message\ResponseInterface							the response
     */
    public function listAction()
    {
		$events = $this->eventRepository->findAll();
		$events = $this->sortingService->sort($events);
		$events = $this->renderService->renderEvents($events);
		$page = $this->getPageFromUrl();
		if ($page>0) {$this->view->assign('page', array('currentPage' => $page));}
		$this->view->assign('target', array('pid' => $this->settings['detail']['pid']));
		$currentContentObject = $this->request->getAttribute('currentContentObject');
		// @extensionScannerIgnoreLine
		if (isset($currentContentObject->data['pid']))
		{
			// @extensionScannerIgnoreLine
			$this->view->assign('source', array('pid' => $currentContentObject->data['pid']));
		}
		else
		{
			// @extensionScannerIgnoreLine
			$this->view->assign('source', array('pid' => $GLOBALS['TSFE']->id));
		}
		$this->view->assign('events', $events);
		$paginationService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Pagination\\PaginationService', $events, $page, $this->settings['paginate']['eventsPerPage'], $this->settings['paginate']['maxLinks']);
		$this->view->assign('pagination', $paginationService->getPaginationArguments());
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }
	
	/**
     * Action for this controller.
	 * list events as list of tt_content items.
     *
     * @return \Psr\Http\Message\ResponseInterface							the response
     */
    public function listDetailAction()
    {
		$events = $this->eventRepository->findAll();
		$events = $this->sortingService->sort($events);
		$events = $this->renderService->renderEvents($events);
		$page = $this->getPageFromUrl();
		if ($page>0) {$this->view->assign('page', array('currentPage' => $page));}
		$this->view->assign('events', $events);
		$paginationService = $this->makeInstance('Maagit\\Maagitevent\\Service\\Pagination\\PaginationService', $events, $page, $this->settings['paginate']['eventsPerPage'], $this->settings['paginate']['maxLinks']);
		$this->view->assign('pagination', $paginationService->getPaginationArguments());
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }

	/**
     * Action for this controller.
	 * detail event.
     *
     * @return \Psr\Http\Message\ResponseInterface							the response
     */
    public function detailAction(string $id=null)
    {
		if ($id == null) {return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));}
		$event = $this->eventRepository->findById($id);
		if ($event != null) {$event = $this->renderService->renderEvent($event);}
		$page = $this->getPageFromUrl();
		if ($page>0) {$this->view->assign('page', array('currentPage' => $page));}
		$this->view->assign('referrer', array('pid' => $this->getReferrerFromUrl()));
		$this->view->assign('event', $event);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
     * get page number from speaking url
     *
     * @return int
     */
	private function getPageFromUrl()
	{
		// get from paginate parameter, if available
		if (!empty($this->request->getArguments()['currentPage']))
		{
			return (int)$this->request->getArguments()['currentPage'];
		}
		
		// get from speaking url
		$request = $GLOBALS['TYPO3_REQUEST'];
		$normalizedParams = $request->getAttribute('normalizedParams');
		$url = $normalizedParams->getRequestUrl();
		$pagePos = strpos($url, '/page-');
		$endPos = strpos($url, '/', $pagePos);
		if (!$pagePos) {return 0;}
		$page = (!$endPos) ? substr($url, $pagePos+1) : substr($url, $pagePos+1, $endPos);
		$page = str_replace('page-', '', $page);

		// make default value, if empty
		$page = (empty($page)) ? 0 : $page;
		$page = (is_numeric($page)) ? $page : 0;

		// return page number
		return (int)$page;
	}

	/**
     * get referrer pid from speaking url
     *
     * @return string
     */
	private function getReferrerFromUrl()
	{
		// get from parameter, if available
		if (!empty($this->request->getArguments()['referrer']))
		{
			return $this->request->getArguments()['referrer'];
		}

		// get from speaking url
		$request = $GLOBALS['TYPO3_REQUEST'];
		$normalizedParams = $request->getAttribute('normalizedParams');
		$url = $normalizedParams->getRequestUrl();
		$sourcePos = strpos($url, '/source-');
		$endPos = strpos($url, '/', $sourcePos);
		if (!$sourcePos) {return '';}
		$source = (!$endPos) ? substr($url, $sourcePos+1) : substr($url, $sourcePos+1, $endPos);
		$source = str_replace('source-', '', $source);
		return $source;
	}
}