<?php
namespace Maagit\Maagitevent\Helper;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Helper
	class:				DivHelper

	description:		Various helper methods for this plugin.

	created:			2020-07-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-03	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed
						2023-12-13	Urs Maag		Added attribute "site" to request
													on getExtbaseRequest()

------------------------------------------------------------------------------------- */


class DivHelper extends \Maagit\Maagitevent\Helper\BaseHelper
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * execute a object method by reflection
     *
     * @param	object		$object			the object to call the method
	 * @param	string		$method			the method name
	 * @param	array		$params			the method parameters
	 * @return	mixed						the method call result
     */
	public function callByReflection(object $object, string $method, array $args)
	{
		$reflectionMethod = new \ReflectionMethod($object, $method);
		$reflectionMethod->setAccessible(true);
		return $reflectionMethod->invokeArgs($object, $args);
	}
	
	/**
     * execute a object method by reflection
     *
     * @param	object		$object			the object to call the method
	 * @param	string		$member			the member name
	 * @return	mixed						the member variable
     */
	public function getMemberByReflection(object $object, string $member)
	{
		$reflectionClass = new \ReflectionClass($object);
		$reflectionProperty = $reflectionClass->getProperty($member);
		$reflectionProperty->setAccessible(true);
		return $reflectionProperty->getValue($object);
	}
	
	/**
     * create a uri
     *
     * @param	int			$pid			the target page uid
	 * @param	array		$params			the url parameters
	 * @param	boolean		$absolute		create a absolute uri?
	 * @param	boolean		$clearCHash		clear cHash parameter?
	 * @return	string						the generated uri
     */
	public function getUri(int $pid, array $params, bool $absolute=false, bool $clearCHash=false)
	{
		// create link
		$uriBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Mvc\\Web\\Routing\\UriBuilder');
		$uriBuilder->setRequest($this->getExtbaseRequest());
		$uri = $uriBuilder->setTargetPageUid($pid)->setArguments($params)->build();
		
		// clear cHash parameter
		if ($clearCHash)
		{
			$uri = (strpos($uri, '&cHash=')===FALSE) ? $uri : substr($uri, 0, strpos($uri, '&cHash='));
		}
		
		// make a absolute uri
		if ($absolute)
		{
			$host = $_SERVER['HTTP_HOST'];
			$protocol = (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])) ? 'https://' : 'http://';
			$uri = $protocol.$host.$uri;
		}
		
		// return generated uri
		return $uri;
	}
	
	/**
     * extracts the only name of a given controller object (without "Controller" suffix)
     *
     * @param	\Maagit\Maagitevent\Controller\BaseController		$controller			the controller object
	 * @return	string																	the name of controller
     */
	public function getControllerName(\Maagit\Maagitevent\Controller\BaseController $controller)
	{
		$className = get_class($controller);
		$name = strtolower(str_replace('Controller', '', substr($className, strrpos($className, '\\') + 1)));
		return $name;
	}
	
	/**
     * translate model name to repository name
     *
     * @param	\Maagit\Maagitevent\Domain\Repository\BaseModel		$model			the model object
	 * @return	string																the repository name
     */
	public function getRepositoryName(\Maagit\Maagitevent\Domain\Model\BaseModel $model)
	{
		$name = get_class($model).'Repository';
		$name = str_replace('\\Model\\', '\\Repository\\', $name);
		return $name;
	}

	/**
     * translate repository name to model name
     *
     * @param	\Maagit\Maagitevent\Domain\Repository\BaseRepository		$repository			the repository object
	 * @return	string																			the model name
     */
	public function getModelName(\Maagit\Maagitevent\Domain\Repository\BaseRepository $repository)
	{
		$name = \TYPO3\CMS\Core\Utility\ClassNamingUtility::translateRepositoryNameToModelName(get_class($repository));
		return $name;
	}

	/**
     * create a secure url with extension "maagitseclink"
     *
     * @param	string		$url			the url to make secure
	 * @param	string		$linkText		the link text
	 * @param	int			$expire			the time in seconds of link expiration
	 * @return	string						the generated uri
     */
	public function getSecureLink(string $url, string $linkText, int $expire)
	{
		$secureLink = '';
		if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('maagitseclink'))
		{
			$content['url'] = $url;
			$content['value'] = $linkText;
			$conf['expireTime'] = $expire;
			$seclinkObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitseclink\\Frontend\\SecureLink');
			$secureLink = $seclinkObj->getSecureLink($content, $conf);
			$secureLink = substr($secureLink, strpos($secureLink, '<a href="')+9, strpos($secureLink, '">'.$content['value'])-(1+strlen($content['value'])));
		}
		return $secureLink;
	}

	/**
     * add array elements to array
	 *
	 * @param	mixed			$elements			a object or a array of objects to add
	 * @param	array			$target				the target array
	 * @return	array								the new array
     */
	public function arrayMerge($elements, array $target)
	{
		if (is_array($elements))
		{
			foreach ($elements as $element)
			{
				if (!in_array($element, $target))
				{
					$target[] = $element;
				}
			}
		}
		else
		{
			if (!empty($elements))
			{
				if (!in_array($elements, $target))
				{
					$target[] = $elements;	
				}
			}
		}
		return $target;
	}

	/**
     * write the generated sql statement to output
     *
     * @param	\TYPO3\CMS\Extbase\Persistence\Generic\Query		$query			the query object
	 * @return	void
     */
	public function debugQuery(\TYPO3\CMS\Extbase\Persistence\Generic\Query $query)
	{
		$queryParser = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Storage\\Typo3DbQueryParser');
		echo '<strong>Query</strong><br />';
		\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($queryParser->convertQueryToDoctrineQueryBuilder($query)->getSQL());
		echo '<br /><br /><strong>Parameter</strong><br />';
		\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($queryParser->convertQueryToDoctrineQueryBuilder($query)->getParameters());
	}

	/**
     * create a core request object
     *
     * @param	-
	 * @return	\TYPO3\CMS\Core\Http\ServerRequestInterface					the request object
     */
	public function getRequest()
	{
	    return \TYPO3\CMS\Core\Http\ServerRequestFactory::fromGlobals();
	}

	/**
     * create a extbase request object
     *
     * @param	string								$controller		the controller class as string
	 * @return	\TYPO3\CMS\Extbase\Mvc\Request						the extbase request object
     */
	public function getExtbaseRequest(string $controller='\Maagit\Maagitevent\Controller\EventController')
	{
		$siteFinder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Site\\SiteFinder');
		$sites = $siteFinder->getAllSites();
		$site = empty($sites['default']) ? ($sites['config'] ?? array_values($sites)[0]) : $sites['default'];
		// @extensionScannerIgnoreLine
		$contentObject = $this->getContentObject();
		$pageInformation = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Frontend\Page\PageInformation::class);
		$pageInformation->setId($contentObject?->data['pid']);
		$extbaseAttribute = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Mvc\ExtbaseRequestParameters::class, $controller);
		$extbaseRequest = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Mvc\Request::class, $this->getRequest()->withAttribute('extbase', $extbaseAttribute)->withAttribute('applicationType', 1)->withAttribute('site', $site)->withAttribute('currentContentObject', $contentObject)->withAttribute('frontend.page.information', $pageInformation));
		return $extbaseRequest;
	}

	/**
     * create a rendering context
     *
     * @param	-
	 * @return	\TYPO3\CMS\Extbase\Mvc\Request						the extbase request object
     */
	public function getRenderingContext()
	{
	    $renderingContext = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextFactory::class)->create();
	    $renderingContext->setRequest($this->getExtbaseRequest());
		return $renderingContext;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Get plugin as contentObject
     *
	 * @param	string				$plugin					the plugin name
	 * @return	array										the flexform values
	 */
	protected function getContentObject(string $plugin='maagitevent_list')
	{
		$queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getQueryBuilderForTable('tt_content');	
		$queryBuilder 
			->select('tt_content.*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->in(
					'tt_content.CType', 
					$queryBuilder->createNamedParameter(array($plugin), \TYPO3\CMS\Core\Database\Connection::PARAM_STR_ARRAY)
				)
		);
		$record = $queryBuilder->executeQuery()->fetchAssociative();
		$contentObject = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer::class);
		if ($record !== FALSE)
		{
			$contentObject->start($record, 'tt_content');
		}
		return $contentObject;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
