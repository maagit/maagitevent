CREATE TABLE tt_content (
	tx_maagitevent_dates varchar(255) DEFAULT '' NOT NULL,
	tx_maagitevent_excepts varchar(255) DEFAULT '' NOT NULL,
	tx_maagitevent_next int(11),
	tx_maagitevent_title varchar(255),
	tx_maagitevent_location varchar(255),
	tx_maagitevent_price double
);

CREATE TABLE tx_maagitevent_domain_model_date (
    uid int(11) unsigned DEFAULT 0 NOT NULL auto_increment,
    pid int(11) DEFAULT 0 NOT NULL,
	
	tt_content int(11) unsigned DEFAULT 0 NOT NULL,
	fromdate int(11),
	individuals varchar(255) DEFAULT '' NOT NULL,
	todate int(11),
	repeat varchar(255),
	every int(11),
	interval varchar(255),
	monday tinyint(1),
	tuesday tinyint(1),
	wednesday tinyint(1),
	thursday tinyint(1),
	friday tinyint(1),
	saturday tinyint(1),
	sunday tinyint(1),
	last tinyint(1),
	location varchar(255),
	price double,
	cancelled tinyint(1),
	full tinyint(1),
	sorting int(10) unsigned DEFAULT 0 NOT NULL,
	sorting_foreign int(10) unsigned DEFAULT 0 NOT NULL,

    tstamp int(11) unsigned DEFAULT 0 NOT NULL,
    crdate int(11) unsigned DEFAULT 0 NOT NULL,
    deleted tinyint(4) unsigned DEFAULT 0 NOT NULL,
    hidden tinyint(4) unsigned DEFAULT 0 NOT NULL,
    sys_language_uid int(11) DEFAULT 0 NOT NULL,
    l18n_parent int(11) DEFAULT 0 NOT NULL,
    l18n_diffsource mediumblob DEFAULT '' NOT NULL,
    access_group int(11) DEFAULT 0 NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),
	KEY tt_content (tt_content)
);

CREATE TABLE tx_maagitevent_domain_model_individual (
    uid int(11) unsigned DEFAULT 0 NOT NULL auto_increment,
    pid int(11) DEFAULT 0 NOT NULL,
	
	tx_maagitevent_domain_model_date int(11) unsigned DEFAULT 0 NOT NULL,
	individualdatefrom int(11),
	individualdateto int(11),
	sorting int(10) unsigned DEFAULT 0 NOT NULL,
	sorting_foreign int(10) unsigned DEFAULT 0 NOT NULL,

    tstamp int(11) unsigned DEFAULT 0 NOT NULL,
    crdate int(11) unsigned DEFAULT 0 NOT NULL,
    deleted tinyint(4) unsigned DEFAULT 0 NOT NULL,
    hidden tinyint(4) unsigned DEFAULT 0 NOT NULL,
    sys_language_uid int(11) DEFAULT 0 NOT NULL,
    l18n_parent int(11) DEFAULT 0 NOT NULL,
    l18n_diffsource mediumblob DEFAULT '' NOT NULL,
    access_group int(11) DEFAULT 0 NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),
	KEY tx_maagitevent_domain_model_date (tx_maagitevent_domain_model_date)
);

CREATE TABLE tx_maagitevent_domain_model_except (
    uid int(11) unsigned DEFAULT 0 NOT NULL auto_increment,
    pid int(11) DEFAULT 0 NOT NULL,
	
	tt_content int(11) unsigned DEFAULT 0 NOT NULL,
	exceptdatefrom int(11),
	exceptdateto int(11),
	sorting int(10) unsigned DEFAULT 0 NOT NULL,
	sorting_foreign int(10) unsigned DEFAULT 0 NOT NULL,

    tstamp int(11) unsigned DEFAULT 0 NOT NULL,
    crdate int(11) unsigned DEFAULT 0 NOT NULL,
    deleted tinyint(4) unsigned DEFAULT 0 NOT NULL,
    hidden tinyint(4) unsigned DEFAULT 0 NOT NULL,
    sys_language_uid int(11) DEFAULT 0 NOT NULL,
    l18n_parent int(11) DEFAULT 0 NOT NULL,
    l18n_diffsource mediumblob DEFAULT '' NOT NULL,
    access_group int(11) DEFAULT 0 NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),
	KEY tt_content (tt_content)
);