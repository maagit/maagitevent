Den RouteEnhancer für sprechende Url's wie folgt einbinden:
===========================================================
1. Inhalt der Datei "config.yaml" in die Zwischenablage kopieren
2. Zwischenablage in die Seitenkonfiguration einfügen (typo3conf/sites/<seitenidentifikation>/config.yaml)
3. Unter "aspects: -> title: -> pluginPid:" die Seiten-ID eintragen, auf der das Event-Plugin eingefügt wurde
4. Cache löschen