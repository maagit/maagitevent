plugin.tx_maagitevent {
	persistence {
		storagePid = {$plugin.tx_maagitevent.persistence.storagePid}
		enableAutomaticCacheClearing = 1		
	}

	view {
		layoutRootPaths {
			300 = {$plugin.tx_maagitevent.view.layoutRootPath}
		}
		templateRootPaths {
			300 = {$plugin.tx_maagitevent.view.templateRootPath}
		}
		partialRootPaths {
			300 = {$plugin.tx_maagitevent.view.partialRootPath}
		}
	}

	settings {
		view < plugin.tx_maagitevent.view
		pageid = 
	}
}

tt_content {
	maagitevent_content < tt_content.textmedia
	maagitevent_content {
		templateName = EventContent.html
	}
	maagitevent_content.settings < plugin.tx_maagitevent.settings
}

lib.contentElement {
	layoutRootPaths =< lib.contentElement.layoutRootPaths
	layoutRootPaths {
		300 = {$plugin.tx_maagitevent.view.layoutRootPath}
	}
	templateRootPaths =< lib.contentElement.templateRootPaths
	templateRootPaths {
		300 = {$plugin.tx_maagitevent.view.templateRootPath}
	}
	partialRootPaths =< lib.contentElement.partialRootPaths
	partialRootPaths {
		300 = {$plugin.tx_maagitevent.view.partialRootPath}
	}
}

plugin.tx_form {
    settings {
        yamlConfigurations {
			1684750583 = EXT:maagitevent/Configuration/Yaml/customElements.yaml
        }
    }
}

module.tx_form {
	settings {
		yamlConfigurations {
			1684750583 	 = EXT:maagitevent/Configuration/Yaml/customElements.yaml
        }
    }
}
