<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date',
        'label' => 'fromdate',
        'label_userFunc' => 'Maagit\\Maagitevent\\Userfuncs\\Tca->dateTitle',
		'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden'
        ],
        'iconfile' => 'EXT:maagitevent/Resources/Public/Icons/event.png'
    ],
    'interface' => [
        'maxDBListItems' => 100,
        'maxSingleDBListItems' => 500
    ],
    'types' => [
        '1' => [
			'showitem' => '
				--div--;LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.tabs.date,
					fromdate, todate,
				--div--;LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.tabs.repeat,
					repeat, every, interval, monday, tuesday, wednesday, thursday, friday, saturday, sunday, last,
				--div--;LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.tabs.individual,
					individuals,
				--div--;LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.tabs.extended,
					location, price, cancelled, full'
        ]
    ],
	'palettes' => [

	],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check'
            ]
        ],
		'fromdate' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.fromdate',
            'config' => [
                'type' => 'datetime',
				'required' => true
            ]
        ],
        'todate' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.todate',
            'config' => [
                'type' => 'datetime'
            ]
        ],
		'individuals' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xlf:tx_maagitevent_domain_model_date.individuals',
			'config' => [
				'type' => 'inline',
				'foreign_table' => 'tx_maagitevent_domain_model_individual',
				'foreign_field' => 'tx_maagitevent_domain_model_date',
				'foreign_sortby' => 'sorting',
				'maxitems' => 100,
				'appearance' => [
					'collapseAll' => true,
					'expandSingle' => true,
					'newRecordLinkAddTitle' => true,
					'useSortable' => true,
					'enabledControls' => [
						'info' => false,
						'new' => true,
						'sort' => false,
						'hide' => true,
						'dragdrop' => true,
						'delete' => true,
						'localize' => false,
					]
				]
			]
		],
        'repeat' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.repeat',
			'onChange' => 'reload',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.repeat.empty',
						'value' => ''
					],
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.repeat.every',
						'value' => 'every'
					]
				]
			]
        ],
        'every' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.every',
			'displayCond' => 'FIELD:repeat:!=:',
			'config' => [
				'type' => 'number',
				'size' => 10,
				'range' => [
					'lower' => 1,
					'upper' => 365,
				],
				'default' => 1,
				'slider' => [
					'step' => 1,
					'width' => 365,
				]
			]
        ],
		'interval' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.interval',
			'onChange' => 'reload',
			'displayCond' => 'FIELD:repeat:!=:',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.interval.day',
						'value' => 'day'
					],
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.interval.week',
						'value' => 'week'
					],
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.interval.month',
						'value' => 'month'
					],
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.interval.year',
						'value' => 'year'
					]
				]
			]
        ],
        'monday' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.monday',
			'displayCond' => [
				'AND' => [
					'FIELD:repeat:!=:',
			        'FIELD:interval:IN:week,month,year',
			    ],
			],
			'config' => [
				'type' => 'check',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.monday',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled'
					]
				]
			]
		],
        'tuesday' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.tuesday',
			'displayCond' => [
				'AND' => [
					'FIELD:repeat:!=:',
			        'FIELD:interval:IN:week,month,year',
			    ],
			],
			'config' => [
				'type' => 'check',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.tuesday',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled'
					]
				]
			]
		],
        'wednesday' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.wednesday',
			'displayCond' => [
				'AND' => [
					'FIELD:repeat:!=:',
			        'FIELD:interval:IN:week,month,year',
			    ],
			],
			'config' => [
				'type' => 'check',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.wednesday',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled'
					]
				]
			]
		],
        'thursday' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.thursday',
			'displayCond' => [
				'AND' => [
					'FIELD:repeat:!=:',
			        'FIELD:interval:IN:week,month,year',
			    ],
			],
			'config' => [
				'type' => 'check',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.thursday',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled'
					]
				]
			]
		],
        'friday' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.friday',
			'displayCond' => [
				'AND' => [
					'FIELD:repeat:!=:',
			        'FIELD:interval:IN:week,month,year',
			    ],
			],
			'config' => [
				'type' => 'check',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.friday',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled'
					]
				]
			]
		],
        'saturday' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.saturday',
			'displayCond' => [
				'AND' => [
					'FIELD:repeat:!=:',
			        'FIELD:interval:IN:week,month,year',
			    ],
			],
			'config' => [
				'type' => 'check',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.saturday',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled'
					]
				]
			]
		],
        'sunday' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.sunday',
			'displayCond' => [
				'AND' => [
					'FIELD:repeat:!=:',
			        'FIELD:interval:IN:week,month,year',
			    ],
			],
			'config' => [
				'type' => 'check',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.sunday',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled'
					]
				]
			]
		],
        'last' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.last',
			'displayCond' => [
				'AND' => [
					'FIELD:repeat:!=:',
			        'FIELD:interval:IN:month',
			    ],
			],
			'config' => [
				'type' => 'check',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.last',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled'
					]
				]
			]
		],
        'location' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.location',
            'config' => [
                'type' => 'input',
				'size' => 50,
                'eval' => 'trim'
            ]
        ],
        'price' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.price',
            'config' => [
                'type' => 'number',
				'format' => 'decimal',
				'size' => 20
            ]
        ],
        'cancelled' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.cancelled',
			'config' => [
				'type' => 'check',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.cancelled',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled'
					]
				]
			]
		],
        'full' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.full',
			'config' => [
				'type' => 'check',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_date.full',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled'
					]
				]
			]
		],
	]
];
