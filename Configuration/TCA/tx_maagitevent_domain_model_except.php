<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_except',
        'label' => 'exceptdatefrom',
		'label_userFunc' => 'Maagit\\Maagitevent\\Userfuncs\\Tca->exceptTitle',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden'
        ],
        'iconfile' => 'EXT:maagitevent/Resources/Public/Icons/event.png'
    ],
    'interface' => [
        'maxDBListItems' => 100,
        'maxSingleDBListItems' => 500
    ],
    'types' => [
        '1' => [
			'showitem' => '
				--div--;LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_except.tabs.except,
					exceptdatefrom, exceptdateto'
        ]
    ],
	'palettes' => [

	],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
			 	'renderType' => 'checkboxToggle',
			 	'items' => [
					 [
					 	'label' => 'Visible',
					 	'labelChecked' => 'Enabled',
					 	'labelUnchecked' => 'Disabled',
					 	'invertStateDisplay' => true
					]
				]
            ]
        ],
		'exceptdatefrom' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_except.exceptdatefrom',
            'config' => [
                'type' => 'datetime',
				'size' => 20,
				'required' => true
            ]
		],
		'exceptdateto' => [
			'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_except.exceptdateto',
			'config' => [
				'type' => 'datetime',
				'size' => 20
			]
		]
	]
];
