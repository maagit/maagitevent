<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_individual',
        'label' => 'individualdatefrom',
        'label_userFunc' => 'Maagit\\Maagitevent\\Userfuncs\\Tca->individualTitle',
		'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden'
        ],
        'iconfile' => 'EXT:maagitevent/Resources/Public/Icons/event.png'
    ],
    'interface' => [
        'maxDBListItems' => 100,
        'maxSingleDBListItems' => 500
    ],
    'types' => [
        '1' => [
			'showitem' => '
				--div--;LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_individual.tabs.individual,
					individualdatefrom, individualdateto'
        ]
    ],
	'palettes' => [

	],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
			 	'renderType' => 'checkboxToggle',
			 	'items' => [
					 [
					 	'label' => 'Visible',
					 	'labelChecked' => 'Enabled',
					 	'labelUnchecked' => 'Disabled',
					 	'invertStateDisplay' => true
					]
				]
            ]
        ],
		'individualdatefrom' => [
            'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_individual.individualdatefrom',
            'config' => [
                'type' => 'datetime',
				'size' => 20,
				'required' => true
            ]
		],
		'individualdateto' => [
			'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_domain_model_individual.individualdateto',
			'config' => [
				'type' => 'datetime',
				'size' => 20
			]
		]
	]
];
