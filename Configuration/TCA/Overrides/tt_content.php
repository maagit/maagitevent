<?php
defined('TYPO3') || die('Access denied.');

// Add register "events" to tt_content's backend form
$GLOBALS['TCA']['tt_content']['types']['maagitevent_content']['showitem'] = '
	'.$GLOBALS['TCA']['tt_content']['types']['textmedia']['showitem'].',
	--div--;LLL:EXT:maagitevent/Resources/Private/Language/locallang.xlf:plugins.title,
		--palette--;;maagitevent_events,
		--palette--;;maagitevent_data,
		--palette--;;maagitevent_behavior
';

// Add palettes to backend form, register "events"
$GLOBALS['TCA']['tt_content']['palettes']['maagitevent_events'] = [
	'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xlf:tx_maagitevent.palettes.events',
	'showitem' => 'tx_maagitevent_dates, --linebreak--, tx_maagitevent_excepts'
];
$GLOBALS['TCA']['tt_content']['palettes']['maagitevent_data'] = [
	'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xlf:tx_maagitevent.palettes.data',
	'showitem' => 'tx_maagitevent_title, tx_maagitevent_location, tx_maagitevent_price'
];
$GLOBALS['TCA']['tt_content']['palettes']['maagitevent_behavior'] = [
	'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xlf:tx_maagitevent.palettes.behavior',
	'showitem' => 'tx_maagitevent_next'
];

// Add type icon class
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['maagitevent_content'] = 'extensions-maagitevent_content';

// register plugin Content
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitevent',																			// extension name
    'Content',																				// plugin name
    'LLL:EXT:maagitevent/Resources/Private/Language/locallang.xlf:plugins.title',			// plugin title
    'extensions-maagitevent_content',														// icon identifier
    'default', 		 																		// group
    'LLL:EXT:maagitevent/Resources/Private/Language/locallang.xlf:plugins.description'		// plugin description
);

// register plugin List
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitevent',																			// extension name
    'List',																					// plugin name
    'LLL:EXT:maagitevent/Resources/Private/Language/locallang.xlf:plugins.list.title',		// plugin title
    'extensions-maagitevent',																// icon identifier
    'maagitevent',  																		// group
    'LLL:EXT:maagitevent/Resources/Private/Language/locallang.xlf:plugins.list.description'	// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagitevent/Configuration/FlexForms/List.xml',
	$pluginSignature
);

// Define new columns
$temporaryColumns = [
	'bodytext' => [
		'config' => [
			'type' => 'text',
			'enableRichtext' => true
		]
	],
	'tx_maagitevent_dates' => [
		'exclude' => 0,
		'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xlf:tx_maagitevent_dates',
		'config' => [
			'type' => 'inline',
			'foreign_table' => 'tx_maagitevent_domain_model_date',
			'foreign_field' => 'tt_content',
			'foreign_sortby' => 'sorting',
			'maxitems' => 100,
			'appearance' => [
				'collapseAll' => true,
				'expandSingle' => true,
				'newRecordLinkAddTitle' => true,
				'useSortable' => true,
				'enabledControls' => [
					'info' => false,
					'new' => true,
					'sort' => false,
					'hide' => true,
					'dragdrop' => true,
					'delete' => true,
					'localize' => false,
				]
			]
		]
	],
	'tx_maagitevent_excepts' => [
		'exclude' => 0,
		'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xlf:tx_maagitevent_excepts',
		'config' => [
			'type' => 'inline',
			'foreign_table' => 'tx_maagitevent_domain_model_except',
			'foreign_field' => 'tt_content',
			'foreign_sortby' => 'sorting',
			'maxitems' => 100,
			'appearance' => [
				'collapseAll' => true,
				'expandSingle' => true,
				'newRecordLinkAddTitle' => true,
				'useSortable' => true,
				'enabledControls' => [
					'info' => false,
					'new' => true,
					'sort' => false,
					'hide' => true,
					'dragdrop' => true,
					'delete' => true,
					'localize' => false,
				]
			]
		]
	],
    'tx_maagitevent_next' => [
        'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_next',
		'config' => [
			'type' => 'number',
			'size' => 10,
			'range' => [
				'lower' => 0,
				'upper' => 2000,
			],
			'default' => 0,
			'slider' => [
				'step' => 1,
				'width' => 365,
			]
		]
    ],
    'tx_maagitevent_title' => [
        'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_title',
        'config' => [
            'type' => 'input',
			'size' => 50,
            'eval' => 'trim'
        ]
    ],
	'tx_maagitevent_location' => [
        'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_location',
        'config' => [
            'type' => 'input',
			'size' => 50,
            'eval' => 'trim'
        ]
    ],
    'tx_maagitevent_price' => [
        'label' => 'LLL:EXT:maagitevent/Resources/Private/Language/locallang_db.xml:tx_maagitevent_price',
        'config' => [
            'type' => 'number',
			'format' => 'decimal',
			'size' => 20
        ]
    ]
];

// Add new columns to tt_content
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tt_content',
        $temporaryColumns
);
?>