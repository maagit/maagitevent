<?php
call_user_func(function () {
	// add as static template (used for correct extbase table mapping)
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('maagitevent', 'Configuration/TypoScript', 'Maagitevent');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('maagitevent', 'Configuration/TypoScript/DefaultStyles', 'Maagitevent CSS Styles (optional)');
});
?>