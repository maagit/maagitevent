<?php
	return [
		'extensions-maagitevent_content' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagitevent/Resources/Public/Icons/tt_content_event.png',
		],
		'extensions-maagitevent' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagitevent/Resources/Public/Icons/event.png',
		]
	];
?>