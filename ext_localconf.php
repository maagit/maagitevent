<?php
defined('TYPO3') || die('Access denied.');

// Load userfuncs when TYPO3 is not in composer mode
if (!defined('TYPO3_COMPOSER_MODE') || !TYPO3_COMPOSER_MODE) {
	require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('maagitevent').'Classes/General.php';
	require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('maagitevent').'Classes/Helper/BaseHelper.php';
	require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('maagitevent').'Classes/Domain/Model/BaseModel.php';
	require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('maagitevent').'Classes/Service/BaseService.php';
	require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('maagitevent').'Classes/Helper/DivHelper.php';
	require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('maagitevent').'Classes/Domain/Model/Event.php';
	require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('maagitevent').'Classes/Service/Date/DateService.php';
	require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('maagitevent').'Classes/Userfuncs/Form.php';
	require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('maagitevent').'Classes/Userfuncs/Tca.php';
}

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitevent',
	'Content',
	[
		\Maagit\Maagitevent\Controller\ContentController::class => 'index'
	],
	[
		
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitevent',
	'List',
	[
		\Maagit\Maagitevent\Controller\EventController::class => 'route,list,listDetail,detail,data'
	],
	[
		\Maagit\Maagitevent\Controller\EventController::class => 'route,list,listDetail,detail,data'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// add custom mapper for generating speaking urls with routeEnhancer
$GLOBALS['TYPO3_CONF_VARS']['SYS']['routing']['aspects']['PersistedEventMapper'] = \Maagit\Maagitevent\Routing\Aspect\PersistedEventMapper::class;

// FORM: register language file of extension tx_form for backend
$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:form/Resources/Private/Language/Database.xlf'][] = 'EXT:maagitevent/Resources/Private/Language/Backend.xlf';
